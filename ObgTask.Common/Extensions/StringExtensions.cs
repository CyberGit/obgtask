﻿using System.Globalization;

namespace ObgTask.Common.Extensions
{
    public static class StringExtensions
    {
        public static string ToFirstLetterUppercase(this string str)
        {
            if (string.IsNullOrEmpty(str)) return string.Empty;
            var strArray = str.ToCharArray();
            strArray[0] = char.ToUpper(strArray[0]);
            return new string(strArray);
        }

        public static string ToTitleCase(this string str)
        {
            return string.IsNullOrEmpty(str) ? string.Empty : CultureInfo.InvariantCulture.TextInfo.ToTitleCase(str);
        }

    }
}
