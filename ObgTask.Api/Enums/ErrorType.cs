﻿using System.Diagnostics.CodeAnalysis;

// ReSharper disable IdentifierTypo
namespace ObgTask.Api.Enums
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum ErrorType
    {
        EXCEPTION,
        SERVICE,
        VALIDATION
    }
}
