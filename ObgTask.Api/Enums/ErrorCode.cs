﻿using System.Diagnostics.CodeAnalysis;

// ReSharper disable IdentifierTypo
namespace ObgTask.Api.Enums
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum ErrorCode
    {
        E_SERVER_INTERNAL_ERROR,
        E_VALIDATION_EMAILADDRESS_EMPTY,
        E_VALIDATION_EMAILADDRESS_INVALID,
        E_VALIDATION_PASSWORD_EMPTY,
        E_VALIDATION_PASSWORD_LENGTH,
        E_USER_REGISTRATION_FAILED,
        E_USER_LOGIN_FAILED,
    }
}
