﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ObgTask.Api.Models.Response;
using ObgTask.Data.Contracts;
using ObgTask.Data.Contracts.Models.GameData;

namespace ObgTask.Api.Controllers
{
    [Route("api/v1/[controller]")]
    public class GameDataController : ApiBaseController
    {
        private readonly IGameDataService _gameService;

        public GameDataController(IGameDataService gameService)
        {
            _gameService = gameService;
        }

        /// GET: api/v1/gameData/54
        [HttpGet("{gameId}")]
        [ProducesResponseType(typeof(Game), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetGame([FromRoute] int gameId)
        {
            var game = await _gameService.GetGameAsync(gameId);
            if (game == null) return NotFound();
            return Ok(game);
        }

        /// GET: api/v1/gameData?pageNumber=0&pageSize=20
        [HttpGet]
        [ProducesResponseType(typeof(IPagedList<Game>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllGames([FromQuery]int? pageNumber, int? pageSize)
        {
            var gamesPagedList = await _gameService.GetGamesAsync(pageNumber, pageSize);
            if (gamesPagedList == null) return NotFound();
            return Ok(gamesPagedList);
        }
    }
}
