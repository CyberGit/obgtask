﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ObgTask.Api.Models.Response;
using ObgTask.Data.Contracts;
using ObgTask.Data.Contracts.Models.GameData;

namespace ObgTask.Api.Controllers
{
    [Route("api/v1/[controller]")]
    public class CollectionsController : ApiBaseController
    {
        private readonly IGameDataService _gameService;

        public CollectionsController(IGameDataService gameService)
        {
            _gameService = gameService;
        }

        /// GET: api/v1/collections/4?excludeSubCollections=false
        [HttpGet("{collectionId}")]
        [ProducesResponseType(typeof(GameCollection), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetCollection([FromRoute] int collectionId, [FromQuery]bool? excludeSubCollections)
        {
            var collection = await _gameService.GetCollectionAsync(collectionId, excludeSubCollections.GetValueOrDefault(false));
            if (collection == null) return NotFound();
            return Ok(collection);
        }

        /// GET: api/v1/collections?pageNumber=0&pageSize=20
        [HttpGet]
        [ProducesResponseType(typeof(IPagedList<GameCollection>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllRootCollection([FromQuery]int? pageNumber, int? pageSize, bool? useEfCoreLazyLoad = false)
        {
            IPagedList<GameCollection> collectionsList;

            // Just to see performance diff
            if (useEfCoreLazyLoad.GetValueOrDefault())
                collectionsList = await _gameService.GetRootCollectionsEfLazyAsync(pageNumber, pageSize);
            else
                collectionsList = await _gameService.GetRootCollectionsAsync(pageNumber, pageSize);

            if (collectionsList == null) return NotFound();
            return Ok(collectionsList);
        }
    }
}
