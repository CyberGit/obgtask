﻿using System.Net.Mime;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ObgTask.Api.Enums;
using ObgTask.Api.Models.Response;

namespace ObgTask.Api.Controllers
{
    [Produces(MediaTypeNames.Application.Json)]
    [ApiController, ApiConventionType(typeof(DefaultApiConventions))]
    [ProducesResponseType(typeof(ApiProblemDetails), StatusCodes.Status500InternalServerError)]
    public abstract class ApiBaseController : ControllerBase
    {
        [ApiExplorerSettings(IgnoreApi = true)]
        protected BadRequestObjectResult ApiProblemDetails(ErrorType errorType, ErrorCode errorCode)
        {
            var details = new ApiProblemDetails(HttpContext, errorType, errorCode)
            {
                Status = StatusCodes.Status400BadRequest
            };
            return new BadRequestObjectResult(details);
        }
    }
}
