﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ObgTask.Api.Models.Request;
using ObgTask.Data.Contracts;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using ObgTask.Api.Enums;
using ObgTask.Api.Extensions;
using ObgTask.Api.Models.Response;
using ObgTask.Data.Contracts.Models;
using ObgTask.Data.DataAccess.Entities;

namespace ObgTask.Api.Controllers
{
    [Route("api/v1/[controller]/[action]")]
    public class AuthController : ApiBaseController
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public AuthController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        /// POST: api/v1/auth/login
        [HttpPost]
        [ProducesResponseType(typeof(JsonWebToken), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Login([FromBody] UserLoginRequest requestModel)
        {
            var user = await _userService.UserManager.FindByEmailAsync(requestModel.Email);
            if (user == null)
                return ApiProblemDetails(ErrorType.SERVICE, ErrorCode.E_USER_LOGIN_FAILED);

            var signInResult = await _userService.SignInManager
                .CheckPasswordSignInAsync(user, requestModel.Password, false);

            if (!signInResult.Succeeded)
                return ApiProblemDetails(ErrorType.SERVICE, ErrorCode.E_USER_LOGIN_FAILED);

            var token = await _userService.GetTokenAsync(user);
            return Ok(token);
        }

        /// POST: api/v1/auth/register
        [HttpPost]
        [ProducesResponseType(typeof(JsonWebToken), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Register([FromBody] UserRegistrationRequest requestModel)
        {
            var user = _mapper.Map<AppUser>(requestModel);
            var registrationResult = await _userService.UserManager.CreateAsync(user, requestModel.Password);

            if (!registrationResult.Succeeded)
                return ApiProblemDetails(ErrorType.SERVICE, ErrorCode.E_USER_REGISTRATION_FAILED);

            var token = await _userService.GetTokenAsync(user);
            return Ok(token);
        }

        /// GET: api/v1/auth/refreshToken
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(JsonWebToken), StatusCodes.Status200OK)]
        public async Task<IActionResult> RefreshToken()
        {
            var email = User.Identity.Name ?? User.EmailFromClaims();
            var user = await _userService.UserManager.FindByEmailAsync(email);
            var token = await _userService.GetTokenAsync(user);
            return Ok(token);
        }

        /// GET: api/v1/auth/isAuthorized
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult IsAuthorized()
        {
            return Ok();
        }
    }
}
