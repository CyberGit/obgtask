﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ObgTask.Data.Contracts;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using ObgTask.Api.Enums;
using ObgTask.Api.Extensions;
using ObgTask.Api.Models.Response;
using ObgTask.Data.Contracts.Models.GamePlay;

namespace ObgTask.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GamePlayController : ApiBaseController
    {
        private readonly IGamePlayService _gamePlayService;

        public GamePlayController(IGamePlayService gamePlayService)
        {
            _gamePlayService = gamePlayService;
        }

        /// GET: api/v1/gamePlay/54
        [HttpGet("{gameId}")]
        [ProducesResponseType(typeof(StartGameResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> StartGame([FromRoute] int gameId)
        {
            if (!Guid.TryParse(User.UserIdFromClaims(), out var userId))
                return ApiProblemDetails(ErrorType.SERVICE, ErrorCode.E_USER_LOGIN_FAILED);

            var response = await _gamePlayService.StartGame(new StartGameRequest
            {
                GameId = gameId, UserId = userId
            });

            if (response == null)
                return NotFound();

            return Ok(response);
        }
    }
}
