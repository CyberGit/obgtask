﻿using System.Net.Mime;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using ObgTask.Api.Models.Response;

namespace ObgTask.Api.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseGlobalExceptionHandler(this IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseExceptionHandler(appBuilder =>
            {
                appBuilder.Run(
                    async context =>
                    {
                        context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                        context.Response.ContentType = MediaTypeNames.Application.Json;

                        var exceptionHandler = context.Features.Get<IExceptionHandlerFeature>();
                        if (exceptionHandler != null)
                        {
                            var exception = exceptionHandler.Error;
                            var problemDetails = new ApiProblemDetails(context, exception, env)
                            {
                                Status = context.Response.StatusCode
                            };
                            await context.Response.WriteAsync(problemDetails.ToString());
                        }
                    });
            });
            return app;
        }
    }
}
