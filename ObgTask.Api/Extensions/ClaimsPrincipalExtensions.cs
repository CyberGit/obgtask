﻿using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace ObgTask.Api.Extensions
{
    internal static class ClaimsPrincipalExtensions
    {
        public static string EmailFromClaims(this ClaimsPrincipal principal)
        {
           var email = principal.Claims.SingleOrDefault(x => x.Properties.Values.Contains(JwtRegisteredClaimNames.UniqueName))?.Value;
           return email;
        }

        public static string UserIdFromClaims(this ClaimsPrincipal principal)
        {
            var guid = principal.Claims.SingleOrDefault(x => x.Properties.Values.Contains(JwtRegisteredClaimNames.Sub))?.Value;
            return guid;
        }
    }
}
