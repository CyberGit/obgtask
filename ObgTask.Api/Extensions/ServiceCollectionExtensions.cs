﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Newtonsoft.Json;
using ObgTask.Api.Models.Response;
using Swashbuckle.AspNetCore.Swagger;

namespace ObgTask.Api.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IMvcBuilder RegisterApiServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "ObgTask.API", Version = "v1" });
                options.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    In = "header",
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = "apiKey"
                });
                options.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", new string[] { } }
                });
            });
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(x => x.FullName.StartsWith("ObgTask"));
            var mvc = services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddFluentValidation(x =>
                {
                    foreach (var assembly in assemblies)
                    {
                        x.RegisterValidatorsFromAssembly(assembly);
                    }
                }).AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                    options.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
                    options.SerializerSettings.DateParseHandling = DateParseHandling.DateTimeOffset;
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });

            // From ASP.NET Core 2.1 internal Model Validator is implemented by default.
            // To use custom model state validator filter we need to suppress default one.
            services.Configure<ApiBehaviorOptions>(options =>
            {
                //options.SuppressModelStateInvalidFilter = true;
                options.SuppressMapClientErrors = true;
                options.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetails = new ApiProblemDetails(context.HttpContext, context.ModelState)
                    {
                        Status = StatusCodes.Status400BadRequest,
                    };
                    return new BadRequestObjectResult(problemDetails);
                };
            });

            return mvc;
        }
    }
}
