﻿using AutoMapper;
using ObgTask.Api.Models.Request;
using ObgTask.Data.DataAccess.Entities;


namespace ObgTask.Api.Mappings
{
    public class ApiMappingProfile : Profile
    {
        public override string ProfileName => GetType().Name;

        public ApiMappingProfile()
        {
            CreateMap<UserRegistrationRequest, AppUser>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email));

            CreateMap<UserLoginRequest, AppUser>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email));
        }
    }
}
