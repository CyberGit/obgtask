﻿using FluentValidation;
using ObgTask.Api.Enums;

namespace ObgTask.Api.Models.Request
{
    public class UserLoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class UserLoginRequestValidator : AbstractValidator<UserLoginRequest>
    {
        public UserLoginRequestValidator()
        {
            RuleFor(request => request.Email)
                .NotEmpty()
                .WithMessage(ErrorCode.E_VALIDATION_EMAILADDRESS_EMPTY.ToString())
                .EmailAddress()
                .WithMessage(ErrorCode.E_VALIDATION_EMAILADDRESS_INVALID.ToString());

            RuleFor(request => request.Password)
                .NotEmpty()
                .WithMessage(ErrorCode.E_VALIDATION_PASSWORD_EMPTY.ToString())
                .MinimumLength(6)
                .WithMessage(ErrorCode.E_VALIDATION_PASSWORD_LENGTH.ToString());
        }
    }
}
