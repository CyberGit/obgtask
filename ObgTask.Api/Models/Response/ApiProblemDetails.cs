﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ObgTask.Api.Enums;

namespace ObgTask.Api.Models.Response
{
    /// <inheritdoc />
    /// <summary>
    /// A machine-readable format for specifying errors in HTTP API responses based on https://tools.ietf.org/html/rfc7807.
    /// </summary>
    internal class ApiProblemDetails : ValidationProblemDetails
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ErrorType ErrorType { get; }

        public ApiProblemDetails(HttpContext ctx, ModelStateDictionary modelState) : base(modelState)
        {
            Title = string.Empty;
            ErrorType = ErrorType.VALIDATION;
            Instance = ctx.Request.Path;
        }

        public ApiProblemDetails(HttpContext ctx, ErrorType errorType, ErrorCode errorCode)
        {
            ErrorType = errorType;
            Title = errorCode.ToString();
            Instance = ctx.Request.Path;
        }

        public ApiProblemDetails(HttpContext ctx, Exception exception, IHostingEnvironment env, 
            ErrorCode errorCode = ErrorCode.E_SERVER_INTERNAL_ERROR)
        {
            ErrorType = ErrorType.EXCEPTION;
            Title = errorCode.ToString();
            Detail = env.IsDevelopment() ? exception.Message : null;
            Instance = ctx.Request.Path;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
