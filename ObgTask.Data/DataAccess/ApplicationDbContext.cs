﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using ObgTask.Data.Contracts.Enums;
using ObgTask.Data.DataAccess.Entities;
using ObgTask.Data.Seeds;

namespace ObgTask.Data.DataAccess
{
    internal class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        public DbSet<GameDao> Games { get; set; }
        public DbSet<GameProviderDao> GameProviders { get; set; }
        public DbSet<GameCollectionDao> GameCollections { get; set; }
        public DbSet<GameCategoryDao> GameCategories { get; set; }
        public DbSet<GamePlaySessionDao> GamePlaySessions { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Configure db tables
            ConfigureIdentity(builder);
            ConfigureGameEntities(builder);

            // Uncomment to generate initial data for migration
            // builder.SeedGameData();
        }

        private static void ConfigureGameEntities(ModelBuilder builder)
        {
            builder.Entity<GameDao>(entity =>
            {
                entity.ToTable("tGames");
                entity.HasKey(x => x.Id);
                entity.Property(x => x.GamePlatformId).HasDefaultValue(GamePlatformType.Unknown);
                entity.Property(x => x.Name).IsRequired().HasMaxLength(256);
                entity.Property(x => x.ThumbnailUrl).HasMaxLength(2048);
            });

            builder.Entity<GameCollectionDao>(entity =>
            {
                entity.ToTable(name: "tGameCollections");
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Name).IsRequired().HasMaxLength(256);
            });

            builder.Entity<GameCollectionMap>(entity =>
            {
                entity.ToTable(name: "tGameCollectionMap");
                entity.HasKey(gc => new { gc.GameId, gc.GameCollectionId });

                entity.HasOne(gc => gc.GameCollection)
                    .WithMany(c => c.GameCollectionMaps)
                    .HasForeignKey(gc => gc.GameCollectionId);

                entity.HasOne(gc => gc.Game)
                    .WithMany(g => g.GameCollectionsMaps)
                    .HasForeignKey(gc => gc.GameId);
            });

            builder.Entity<GameCategoryDao>(entity =>
            {
                entity.ToTable(name: "tGameCategories");
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Name).IsRequired().HasMaxLength(256);
            });

            builder.Entity<GameProviderDao>(entity =>
            {
                entity.ToTable("tGameProviders");
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Name).IsRequired().HasMaxLength(256);
            });

            builder.Entity<GamePlatformDao>(entity =>
            {
                entity.ToTable("tGamePlatforms");
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Type).HasConversion(
                    ct => ct.ToString(), 
                    cf => Enum.Parse<GamePlatformType>(cf))
                    .IsRequired()
                    .HasMaxLength(256);
            });

            builder.Entity<GamePlaySessionDao>(entity =>
            {
                entity.ToTable("tGamePlaySessions");
                entity.HasKey(x => x.Id);
                entity.Property(p => p.Id).HasMaxLength(449)
                    .HasConversion(ct => ct.ToString(), cf => Guid.Parse(cf));
                entity.HasIndex(p => p.ExpiresAtTime).ForSqlServerIsClustered(false);
                entity.Property(p => p.ExpiresAtTime).IsRequired();
                entity.Property(p => p.StartedAtTime).IsRequired();
                entity.Property(p => p.GameId).IsRequired();
            });
        }

        private static void ConfigureIdentity(ModelBuilder builder)
        {
            builder.Entity<AppUser>(entity => { entity.ToTable("tUsers"); });
            builder.Entity<IdentityRole>(entity => { entity.ToTable(name: "tRoles"); });
            builder.Entity<IdentityUserRole<string>>(entity => { entity.ToTable("tUserRoles"); });
            builder.Entity<IdentityUserClaim<string>>(entity => { entity.ToTable("tUserClaims"); });
            builder.Entity<IdentityUserLogin<string>>(entity => { entity.ToTable("tUserLogins"); });
            builder.Entity<IdentityUserToken<string>>(entity => { entity.ToTable("tUserTokens"); });
            builder.Entity<IdentityRoleClaim<string>>(entity => { entity.ToTable("tRoleClaims"); });
        }

    }
}
