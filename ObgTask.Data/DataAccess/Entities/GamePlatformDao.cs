﻿using System.Collections.Generic;
using ObgTask.Data.Contracts.Enums;

namespace ObgTask.Data.DataAccess.Entities
{
    /// <summary>
    /// Game platform version (desktop, mobile etc.)
    /// </summary>
    internal class GamePlatformDao : BaseEntity<int>
    {
        public GamePlatformType Type { get; set; }
        public ICollection<GameDao> Games { get; set; } = new List<GameDao>();
    }
}
