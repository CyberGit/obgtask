﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Infrastructure;


namespace ObgTask.Data.DataAccess.Entities
{
    internal class GameCollectionDao : BaseEntity<int>
    {
        private readonly ILazyLoader _lazyLoader;
        public GameCollectionDao(ILazyLoader lazyLoader)
        {
            _lazyLoader = lazyLoader;
        }
        public GameCollectionDao()
        {
        }

        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int? ParentCollectionId { get; set; }
        public GameCollectionDao ParentCollection { get; set; }

        private ICollection<GameCollectionMap> _gameCollectionMaps;
        public ICollection<GameCollectionMap> GameCollectionMaps
        {
            get
            {
                try { return _lazyLoader.Load(this, ref _gameCollectionMaps); }
                catch { return null; }
            }
            set => _gameCollectionMaps = value;
        }

        private ICollection<GameCollectionDao> _subCollections;
        public ICollection<GameCollectionDao> SubCollections
        {
            get
            {
                try { return _lazyLoader.Load(this, ref _subCollections); }
                catch { return null; }
            }
            set => _subCollections = value;
        }
    }
}
