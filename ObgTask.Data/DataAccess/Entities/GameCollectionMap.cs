﻿using System;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace ObgTask.Data.DataAccess.Entities
{
    internal class GameCollectionMap
    {
        public int GameId { get; set; }

        public GameDao Game { get; set; }

        public int GameCollectionId { get; set; }
        public GameCollectionDao GameCollection { get; set; }
    }
}
