﻿using System.Collections.Generic;

namespace ObgTask.Data.DataAccess.Entities
{
    /// <summary>
    ///     Defining the game provider in db.
    /// </summary>
    internal class GameProviderDao : BaseEntity<int>
    {
        public string Name { get; set; }
        public ICollection<GameDao> Games { get; set; } = new List<GameDao>();
    }
}
