﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ObgTask.Data.DataAccess.Entities
{
    internal class GamePlaySessionDao : BaseEntity<Guid>
    {
        public Guid UserId { get; set; }
        public DateTimeOffset StartedAtTime { get; set; }
        public DateTimeOffset ExpiresAtTime { get; set; }
        public int GameId { get; set; }

        public GamePlaySessionDao()
        {
            Id = Guid.NewGuid();
            StartedAtTime = DateTime.UtcNow;
            ExpiresAtTime = StartedAtTime.AddMinutes(5);
        }

        public GamePlaySessionDao(int gameId, Guid userId) : this()
        {
            GameId = gameId;
            UserId = userId;
        }
    }
}
