﻿using System;
using System.Collections.Generic;

namespace ObgTask.Data.DataAccess.Entities
{
    internal class GameDao : BaseEntity<int>
    {
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string ThumbnailUrl { get; set; }

        public int GameCategoryId { get; set; }
        public GameCategoryDao GameCategory { get; set; }

        public int GamePlatformId { get; set; }
        public GamePlatformDao GamePlatform { get; set; }

        public int GameProviderId { get; set; }
        public GameProviderDao GameProvider { get; set; }

        public ICollection<GameCollectionMap> GameCollectionsMaps { get; set; } = new List<GameCollectionMap>();

    }
}
