﻿namespace ObgTask.Data.DataAccess.Entities
{
    internal interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }
}
