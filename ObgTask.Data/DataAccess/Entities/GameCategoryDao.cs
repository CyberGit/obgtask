﻿namespace ObgTask.Data.DataAccess.Entities
{
    /// <summary>
    /// classic slots, video slots, roulette, live roulette, poker, blackjack
    /// </summary>
    internal class GameCategoryDao : BaseEntity<int>
    {
        public string Name { get; set; }
    }
}
