﻿using System;

namespace ObgTask.Data.DataAccess.Entities
{
    /// <summary>Represents a base types for dao entities</summary>
    /// <typeparam name="TKey">The type used for the primary key of the entity.</typeparam>
    internal abstract class BaseEntity<TKey> : IEntity<TKey> where TKey : IEquatable<TKey>
    {
        public TKey Id { get; set; }
    }
}
