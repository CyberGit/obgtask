﻿using Microsoft.AspNetCore.Identity;

namespace ObgTask.Data.DataAccess.Entities
{
    public class AppUser : IdentityUser, IEntity<string>
    {
    }
}
