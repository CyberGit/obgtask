﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ObgTask.Data.Contracts;
using ObgTask.Data.Contracts.Enums;
using ObgTask.Data.Contracts.Models.GamePlay;
using ObgTask.Data.DataAccess.Entities;
using ObgTask.Data.Repositories;

namespace ObgTask.Data.Services
{
    internal class GamePlayService : IGamePlayService
    {
        private readonly GamePlayRepository _gamePlayRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GamePlayService(GamePlayRepository gamePlayRepository, IUnitOfWork unitOfWork)
        {
            _gamePlayRepository = gamePlayRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<StartGameResponse> StartGame(StartGameRequest request)
        {
            var session = await GetGameSessionAsync(request.UserId, request.GameId, true);
            if (session != null)
                return new StartGameResponse(session, GameStartStatus.GAME_SESSION_EXISTS_ERROR);

            session = await StartGameSessionAsync(request.UserId, request.GameId);
            if (session != null)
                return new StartGameResponse(session, GameStartStatus.GAME_SESSION_START_SUCCESS);

            return new StartGameResponse(session, GameStartStatus.GAME_SESSION_START_ERROR);
        }

        private async Task<GamePlaySessionDao> StartGameSessionAsync(Guid userId, int gameId)
        {
            var newSessionDao = new GamePlaySessionDao(gameId, userId);
            await _gamePlayRepository.InsertAsync(newSessionDao);
            return await _unitOfWork.SaveChangesAsync() == 1 ? newSessionDao : null;
        }

        private async Task<GamePlaySessionDao> GetGameSessionAsync(Guid userId, int gameId, bool disableTracking)
        {
            var session = await _gamePlayRepository.GetFirstOrDefaultAsync(
                selector: s => s,
                predicate: p => DateTimeOffset.UtcNow <= p.ExpiresAtTime && p.UserId == userId && p.GameId == gameId,
                orderBy: null, include: null, disableTracking: disableTracking);

            return session;
        }
    }
}
