﻿using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using ObgTask.Data.Contracts;
using ObgTask.Data.Contracts.Models;
using ObgTask.Data.DataAccess.Entities;
using JwtIssuerOptions = ObgTask.Data.Contracts.Options.JwtIssuerOptions;

namespace ObgTask.Data.Services
{
    internal class UserService : IUserService
    {
        private readonly JwtIssuerOptions _jwtIssuerOptions;
        public UserManager<AppUser> UserManager { get; }
        public SignInManager<AppUser> SignInManager { get; }

        public UserService(UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            IOptions<JwtIssuerOptions> jwtIssuerOptions)
        {
            _jwtIssuerOptions = jwtIssuerOptions.Value;
            UserManager = userManager;
            SignInManager = signInManager;
        }


        public async Task<JsonWebToken> GetTokenAsync(AppUser appUser)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, appUser.Id),
                new Claim(JwtRegisteredClaimNames.UniqueName, appUser.UserName),

                new Claim(JwtRegisteredClaimNames.Jti, await _jwtIssuerOptions.JtiGenerator.Invoke()),
                new Claim(JwtRegisteredClaimNames.Iat, _jwtIssuerOptions.IssuedAt.ToString(CultureInfo.InvariantCulture))
            };

            var jwt = new JwtSecurityToken(
                signingCredentials: _jwtIssuerOptions.SigningCredentials,
                claims: claims,
                notBefore: _jwtIssuerOptions.NotBefore,
                expires: _jwtIssuerOptions.Expiration,
                audience: _jwtIssuerOptions.Audience,
                issuer: _jwtIssuerOptions.Issuer
            );

            var token = new JwtSecurityTokenHandler().WriteToken(jwt);

            return new JsonWebToken(token, _jwtIssuerOptions.ValidFor.Minutes);
        }
    }
}