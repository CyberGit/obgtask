﻿using System;
using Microsoft.EntityFrameworkCore;
using ObgTask.Data.Contracts;
using ObgTask.Data.DataAccess.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Query;
using ObgTask.Common.Extensions;
using ObgTask.Data.Contracts.Models.GameData;
using ObgTask.Data.Extensions;
using ObgTask.Data.Repositories;

namespace ObgTask.Data.Services
{
    internal class GameDataService : IGameDataService
    {
        private readonly GameRepository _gameRepository;
        private readonly GameCollectionRepository _gameCollectionRepository;
        private readonly GameCollectionMapRepository _gameCollectionMapRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public GameDataService(
            GameRepository gameRepository, 
            GameCollectionRepository gameCollectionRepository, 
            GameCollectionMapRepository gameCollectionMapRepository,
            IUnitOfWork unitOfWork, IMapper mapper)
        {
            _gameRepository = gameRepository;
            _gameCollectionRepository = gameCollectionRepository;
            _gameCollectionMapRepository = gameCollectionMapRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Game> GetGameAsync(int gameId)
        {
            GameDao gameDao = await _gameRepository.GetFirstOrDefaultAsync(
                selector: s => s,
                predicate: p => p.Id == gameId,
                orderBy: o => o.OrderBy(x => x.SortOrder),
                include: game =>
                {
                    return game.IncludeMultiple(p => p.GameProvider, p => p.GameCategory, p => p.GamePlatform,
                        p => p.GameCollectionsMaps).Include(p => p.GameCollectionsMaps);
                }, disableTracking: true);

            if (gameDao == null) return null;

            Game result = _mapper.Map<Game>(gameDao);
            return result;
        }

        public async Task<IPagedList<Game>> GetGamesAsync(int? pageIndex, int? pageSize)
        {
            int index = pageIndex.GetValueOrDefault(0);
            int size = pageSize ?? _gameRepository.Count();

            IPagedList<GameDao> gameDaoList = await _gameRepository.GetPagedListAsync(
                selector: s => s, 
                predicate: null, 
                orderBy:o => o.OrderBy(x => x.SortOrder),
                include: game =>
                {
                    return game.IncludeMultiple(p => p.GameProvider, p => p.GameCategory, p => p.GamePlatform)
                        .Include(p => p.GameCollectionsMaps);
                }, pageIndex: index,pageSize: size, disableTracking: true, cancellationToken: default(CancellationToken));

            if (gameDaoList == null) return null;

            var result = _mapper.Map<IPagedList<Game>>(gameDaoList);
            return result;
        }

        public async Task<GameCollection> GetCollectionAsync(int collectionId, bool excludeSubCollections = false)
        {
            Func<IQueryable<GameCollectionDao>, IIncludableQueryable<GameCollectionDao, object>> IncludeAllOrOnlyGames()
            {
                if(excludeSubCollections)
                    return col => { return col.Include(x => x.GameCollectionMaps); };
                return null;
            }

            var collectionDao = await _gameCollectionRepository.GetFirstOrDefaultAsync(
                selector: s => s,
                predicate: p => p.Id == collectionId, 
                orderBy: o => o.OrderBy(x => x.SortOrder),
                include: IncludeAllOrOnlyGames(), disableTracking: excludeSubCollections);

            if (collectionDao == null) return null;

            GameCollection resultCollection = _mapper.Map<GameCollection>(collectionDao);
            return resultCollection;
        }

        /// <summary>
        /// Does the same as GetRootCollectionsAsync but uses EF core Lazy Load with a lot of querys
        /// </summary>
        public async Task<IPagedList<GameCollection>> GetRootCollectionsEfLazyAsync(int? pageIndex, int? pageSize)
        {
            int index = pageIndex.GetValueOrDefault(0);
            int size = pageSize ?? _gameCollectionRepository.Count(p => p.ParentCollectionId == null);

            var collectionDaoList = await _gameCollectionRepository.GetPagedListAsync(
                selector: s => s,
                predicate: p => p.ParentCollectionId == null,
                orderBy: o => o.OrderBy(x => x.SortOrder),
                include: null,
                pageIndex: index, pageSize: size, disableTracking: false, cancellationToken: default(CancellationToken));

            if (collectionDaoList == null) return null;

            var result = _mapper.Map<IPagedList<GameCollection>>(collectionDaoList);
            return result;
        }


        public async Task<IPagedList<GameCollection>> GetRootCollectionsAsync(int? pageIndex, int? pageSize)
        {
            int index = pageIndex.GetValueOrDefault(0);
            int size = pageSize  ?? _gameCollectionRepository.Count(p => p.ParentCollectionId == null);

            var collectionsList = await _gameCollectionRepository.GetGameCollectionsAsync();
            var gamesList = await _gameCollectionMapRepository.GetGroupedGamesListMapAsync();

            // convert flat collectionsList and gamesList to hierarchical model
            IPagedList<GameCollection> hierarchicalObjects =
                collectionsList.RecursiveJoin<GameCollectionDao, int, GameCollection>(parent => parent.Id,
                    child => child.ParentCollectionId.GetValueOrDefault(0),
                    (dao, children) => {
                        return new GameCollection
                        {
                            Id = dao.Id,
                            Name = dao.Name,
                            SortOrder = dao.SortOrder,
                            ParentId = dao.ParentCollectionId.GetValueOrDefault(0),
                            Games = gamesList.FirstOrDefault(x => x.Key == dao.Id)?.Select(x => x.GameId).ToList(),
                            SubCollections = children.ToList()
                        };
                    }).ToPagedList(index, size);

            if (hierarchicalObjects == null) return null;

            var result = _mapper.Map<IPagedList<GameCollection>>(hierarchicalObjects);
            return result;
        }


    }
}
