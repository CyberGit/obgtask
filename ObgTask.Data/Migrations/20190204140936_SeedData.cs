﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ObgTask.Data.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "tGameCategories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Classic Slots" },
                    { 2, "Video Slots" },
                    { 3, "Roulette" },
                    { 4, "Live Roulette" },
                    { 5, "Poker" },
                    { 6, "Blackjack" }
                });

            migrationBuilder.InsertData(
                table: "tGameCollections",
                columns: new[] { "Id", "Name", "ParentCollectionId", "SortOrder" },
                values: new object[,]
                {
                    { 21, "Assumenda Collection", null, 21 },
                    { 20, "Corrupti Collection", null, 20 },
                    { 15, "Optio Collection", null, 15 },
                    { 14, "Sed Collection", null, 14 },
                    { 9, "Minus Collection", null, 9 },
                    { 8, "Qui Collection", null, 8 },
                    { 1, "Reprehenderit Collection", null, 1 }
                });

            migrationBuilder.InsertData(
                table: "tGamePlatforms",
                columns: new[] { "Id", "Type" },
                values: new object[,]
                {
                    { 1, "Unknown" },
                    { 2, "Desktop" },
                    { 3, "Mobile" }
                });

            migrationBuilder.InsertData(
                table: "tGameProviders",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "NetEntCasino" },
                    { 2, "QuickspinCasino" },
                    { 3, "NyxCasino" },
                    { 4, "Evolution" }
                });

            migrationBuilder.InsertData(
                table: "tGameCollections",
                columns: new[] { "Id", "Name", "ParentCollectionId", "SortOrder" },
                values: new object[,]
                {
                    { 2, "Iusto Collection", 1, 2 },
                    { 3, "Aut Collection", 1, 3 },
                    { 18, "Molestiae Collection", 9, 18 },
                    { 22, "Qui Collection", 9, 22 },
                    { 23, "Id Collection", 9, 23 }
                });

            migrationBuilder.InsertData(
                table: "tGames",
                columns: new[] { "Id", "GameCategoryId", "GamePlatformId", "GameProviderId", "Name", "ReleaseDate", "SortOrder", "ThumbnailUrl" },
                values: new object[,]
                {
                    { 369, 3, 1, 3, "Repellendus Possimus Magnam", new DateTime(2019, 2, 10, 18, 27, 11, 453, DateTimeKind.Utc).AddTicks(2397), 369, "https://loremflickr.com/320/240/casino,casinogame/any?lock=514533297" },
                    { 372, 1, 1, 3, "Placeat Doloremque", new DateTime(2019, 2, 7, 5, 14, 2, 221, DateTimeKind.Utc).AddTicks(9645), 372, "https://loremflickr.com/320/240/casino,casinogame/any?lock=944002107" },
                    { 373, 2, 3, 3, "Voluptatem Quia", new DateTime(2019, 2, 11, 12, 29, 19, 328, DateTimeKind.Utc).AddTicks(4390), 373, "https://loremflickr.com/320/240/casino,casinogame/any?lock=300449357" },
                    { 375, 6, 1, 3, "Rerum", new DateTime(2019, 2, 10, 3, 16, 56, 20, DateTimeKind.Utc).AddTicks(2574), 375, "https://loremflickr.com/320/240/casino,casinogame/any?lock=798363901" },
                    { 377, 2, 1, 3, "Omnis Sunt", new DateTime(2019, 2, 11, 9, 58, 14, 101, DateTimeKind.Utc).AddTicks(8259), 377, "https://loremflickr.com/320/240/casino,casinogame/any?lock=643568918" },
                    { 382, 3, 1, 3, "Magni Molestiae", new DateTime(2019, 2, 7, 16, 48, 28, 10, DateTimeKind.Utc).AddTicks(1172), 382, "https://loremflickr.com/320/240/casino,casinogame/any?lock=714395352" },
                    { 381, 1, 1, 3, "Eaque Alias Non", new DateTime(2019, 2, 10, 4, 52, 57, 239, DateTimeKind.Utc).AddTicks(898), 381, "https://loremflickr.com/320/240/casino,casinogame/any?lock=892754554" },
                    { 367, 1, 3, 3, "Ut Qui", new DateTime(2019, 2, 12, 12, 11, 10, 534, DateTimeKind.Utc).AddTicks(5942), 367, "https://loremflickr.com/320/240/casino,casinogame/any?lock=599399256" },
                    { 385, 2, 3, 3, "Perferendis", new DateTime(2019, 2, 6, 16, 56, 22, 574, DateTimeKind.Utc).AddTicks(7286), 385, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1829738087" },
                    { 388, 1, 1, 3, "Nisi Voluptate Harum", new DateTime(2019, 2, 7, 15, 42, 28, 403, DateTimeKind.Utc).AddTicks(3818), 388, "https://loremflickr.com/320/240/casino,casinogame/any?lock=524768105" },
                    { 396, 4, 1, 3, "Rerum Qui Ex", new DateTime(2019, 2, 11, 21, 15, 55, 718, DateTimeKind.Utc).AddTicks(8575), 396, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2076215005" },
                    { 380, 4, 3, 3, "Fugiat Magnam", new DateTime(2019, 2, 10, 3, 1, 11, 56, DateTimeKind.Utc).AddTicks(1853), 380, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1530541261" },
                    { 355, 1, 1, 3, "Voluptatem Iure", new DateTime(2019, 2, 7, 9, 45, 3, 807, DateTimeKind.Utc).AddTicks(7833), 355, "https://loremflickr.com/320/240/casino,casinogame/any?lock=835707682" },
                    { 341, 5, 1, 3, "Temporibus Officiis Veritatis", new DateTime(2019, 2, 9, 23, 56, 28, 523, DateTimeKind.Utc).AddTicks(4442), 341, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1086087863" },
                    { 346, 1, 1, 3, "Est Tempore Ad", new DateTime(2019, 2, 9, 22, 44, 19, 228, DateTimeKind.Utc).AddTicks(5083), 346, "https://loremflickr.com/320/240/casino,casinogame/any?lock=396633052" },
                    { 400, 1, 1, 3, "Sunt", new DateTime(2019, 2, 11, 12, 41, 53, 351, DateTimeKind.Utc).AddTicks(503), 400, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1734939357" },
                    { 330, 3, 1, 3, "Nemo Quae", new DateTime(2019, 2, 13, 16, 11, 0, 829, DateTimeKind.Utc).AddTicks(8340), 330, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1587523253" },
                    { 326, 5, 1, 3, "Non", new DateTime(2019, 2, 10, 9, 38, 9, 910, DateTimeKind.Utc).AddTicks(8209), 326, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1624188285" },
                    { 325, 1, 1, 3, "Tempore Quaerat Officia", new DateTime(2019, 2, 5, 4, 37, 7, 320, DateTimeKind.Utc).AddTicks(1791), 325, "https://loremflickr.com/320/240/casino,casinogame/any?lock=490558667" },
                    { 324, 3, 2, 3, "Suscipit", new DateTime(2019, 2, 5, 8, 53, 33, 362, DateTimeKind.Utc).AddTicks(6578), 324, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1595098605" },
                    { 320, 6, 3, 3, "Exercitationem Nostrum Vitae", new DateTime(2019, 2, 7, 15, 25, 26, 699, DateTimeKind.Utc).AddTicks(4441), 320, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1058810514" },
                    { 316, 6, 3, 3, "Omnis Aut", new DateTime(2019, 2, 14, 8, 57, 26, 777, DateTimeKind.Utc).AddTicks(414), 316, "https://loremflickr.com/320/240/casino,casinogame/any?lock=391511066" },
                    { 311, 3, 2, 3, "Qui Dolor Est", new DateTime(2019, 2, 12, 11, 15, 31, 797, DateTimeKind.Utc).AddTicks(2184), 311, "https://loremflickr.com/320/240/casino,casinogame/any?lock=398869417" },
                    { 308, 3, 1, 3, "Sit Eius", new DateTime(2019, 2, 7, 23, 50, 13, 338, DateTimeKind.Utc).AddTicks(5763), 308, "https://loremflickr.com/320/240/casino,casinogame/any?lock=466847784" },
                    { 306, 1, 3, 3, "Cum Cupiditate", new DateTime(2019, 2, 9, 12, 34, 13, 102, DateTimeKind.Utc).AddTicks(1203), 306, "https://loremflickr.com/320/240/casino,casinogame/any?lock=242685454" },
                    { 305, 6, 1, 3, "Qui", new DateTime(2019, 2, 11, 13, 7, 52, 473, DateTimeKind.Utc).AddTicks(8026), 305, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1757059462" },
                    { 303, 2, 2, 3, "Aut Illo Voluptatum", new DateTime(2019, 2, 9, 11, 7, 59, 390, DateTimeKind.Utc).AddTicks(4577), 303, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1717356084" },
                    { 352, 5, 1, 3, "Fugit", new DateTime(2019, 2, 9, 10, 48, 15, 338, DateTimeKind.Utc).AddTicks(8431), 352, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2098943361" },
                    { 403, 3, 2, 3, "Ut", new DateTime(2019, 2, 10, 16, 15, 31, 205, DateTimeKind.Utc).AddTicks(3808), 403, "https://loremflickr.com/320/240/casino,casinogame/any?lock=435274595" },
                    { 413, 1, 1, 3, "Itaque", new DateTime(2019, 2, 10, 10, 25, 17, 563, DateTimeKind.Utc).AddTicks(6205), 413, "https://loremflickr.com/320/240/casino,casinogame/any?lock=662502165" },
                    { 297, 2, 2, 3, "Eligendi", new DateTime(2019, 2, 6, 8, 7, 47, 510, DateTimeKind.Utc).AddTicks(6600), 297, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1686601316" },
                    { 28, 3, 1, 4, "Nisi", new DateTime(2019, 2, 7, 11, 47, 48, 84, DateTimeKind.Utc).AddTicks(6267), 28, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2012683529" },
                    { 26, 2, 1, 4, "Cum Impedit", new DateTime(2019, 2, 8, 11, 20, 9, 219, DateTimeKind.Utc).AddTicks(4599), 26, "https://loremflickr.com/320/240/casino,casinogame/any?lock=834318281" },
                    { 11, 4, 3, 4, "Laudantium Deleniti Molestiae", new DateTime(2019, 2, 8, 4, 30, 3, 916, DateTimeKind.Utc).AddTicks(4809), 11, "https://loremflickr.com/320/240/casino,casinogame/any?lock=285807225" },
                    { 6, 5, 3, 4, "Magni Veritatis", new DateTime(2019, 2, 12, 23, 57, 45, 785, DateTimeKind.Utc).AddTicks(1899), 6, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1048867690" },
                    { 1, 2, 3, 4, "Repudiandae Recusandae Eveniet", new DateTime(2019, 2, 5, 4, 1, 32, 609, DateTimeKind.Utc).AddTicks(6962), 1, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1581863019" },
                    { 496, 4, 2, 3, "Aut Dicta", new DateTime(2019, 2, 11, 11, 18, 57, 434, DateTimeKind.Utc).AddTicks(8839), 496, "https://loremflickr.com/320/240/casino,casinogame/any?lock=967160407" },
                    { 492, 3, 2, 3, "Vel Tempora Earum", new DateTime(2019, 2, 9, 5, 9, 34, 910, DateTimeKind.Utc).AddTicks(6813), 492, "https://loremflickr.com/320/240/casino,casinogame/any?lock=464513287" },
                    { 491, 1, 3, 3, "Cumque Dolor", new DateTime(2019, 2, 11, 8, 28, 49, 153, DateTimeKind.Utc).AddTicks(8681), 491, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1467168765" },
                    { 486, 1, 1, 3, "Asperiores Sunt", new DateTime(2019, 2, 5, 21, 54, 41, 650, DateTimeKind.Utc).AddTicks(590), 486, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1568452536" },
                    { 485, 1, 2, 3, "Facere Error", new DateTime(2019, 2, 7, 22, 24, 18, 265, DateTimeKind.Utc).AddTicks(8807), 485, "https://loremflickr.com/320/240/casino,casinogame/any?lock=456537379" },
                    { 480, 2, 1, 3, "Et Vel Quia", new DateTime(2019, 2, 8, 6, 0, 20, 768, DateTimeKind.Utc).AddTicks(4293), 480, "https://loremflickr.com/320/240/casino,casinogame/any?lock=263200695" },
                    { 471, 6, 1, 3, "Pariatur", new DateTime(2019, 2, 11, 14, 18, 13, 587, DateTimeKind.Utc).AddTicks(6189), 471, "https://loremflickr.com/320/240/casino,casinogame/any?lock=551698862" },
                    { 466, 6, 3, 3, "Illum", new DateTime(2019, 2, 6, 3, 39, 50, 444, DateTimeKind.Utc).AddTicks(9040), 466, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1349213665" },
                    { 465, 5, 2, 3, "Qui", new DateTime(2019, 2, 6, 0, 22, 2, 674, DateTimeKind.Utc).AddTicks(2232), 465, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1798314065" },
                    { 454, 5, 1, 3, "Qui", new DateTime(2019, 2, 7, 7, 23, 5, 232, DateTimeKind.Utc).AddTicks(5023), 454, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1626227738" },
                    { 452, 6, 3, 3, "Aut Et", new DateTime(2019, 2, 9, 0, 43, 23, 160, DateTimeKind.Utc).AddTicks(241), 452, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1543424850" },
                    { 450, 1, 2, 3, "Amet", new DateTime(2019, 2, 10, 12, 17, 17, 415, DateTimeKind.Utc).AddTicks(1190), 450, "https://loremflickr.com/320/240/casino,casinogame/any?lock=22202972" },
                    { 445, 6, 1, 3, "Dolores Aut Quia", new DateTime(2019, 2, 7, 20, 57, 22, 300, DateTimeKind.Utc).AddTicks(73), 445, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1735213749" },
                    { 440, 3, 3, 3, "Et", new DateTime(2019, 2, 13, 19, 58, 26, 891, DateTimeKind.Utc).AddTicks(5882), 440, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1514945276" },
                    { 435, 3, 3, 3, "Error Aut", new DateTime(2019, 2, 12, 9, 15, 9, 153, DateTimeKind.Utc).AddTicks(859), 435, "https://loremflickr.com/320/240/casino,casinogame/any?lock=66886506" },
                    { 432, 4, 1, 3, "Sint", new DateTime(2019, 2, 12, 0, 18, 53, 251, DateTimeKind.Utc).AddTicks(1388), 432, "https://loremflickr.com/320/240/casino,casinogame/any?lock=838395095" },
                    { 431, 1, 1, 3, "Vitae Facilis", new DateTime(2019, 2, 9, 3, 42, 13, 707, DateTimeKind.Utc).AddTicks(6316), 431, "https://loremflickr.com/320/240/casino,casinogame/any?lock=816567918" },
                    { 429, 5, 2, 3, "Iste Sit", new DateTime(2019, 2, 8, 0, 36, 27, 600, DateTimeKind.Utc).AddTicks(9872), 429, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1073943436" },
                    { 428, 1, 1, 3, "Sed Tempora", new DateTime(2019, 2, 12, 14, 39, 17, 565, DateTimeKind.Utc).AddTicks(7794), 428, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1593628074" },
                    { 422, 4, 3, 3, "Iusto", new DateTime(2019, 2, 9, 6, 59, 31, 824, DateTimeKind.Utc).AddTicks(276), 422, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1221099567" },
                    { 419, 5, 1, 3, "Officiis Eos", new DateTime(2019, 2, 6, 21, 10, 53, 599, DateTimeKind.Utc).AddTicks(3080), 419, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1198847462" },
                    { 418, 1, 1, 3, "Quae Voluptas Incidunt", new DateTime(2019, 2, 11, 16, 37, 28, 385, DateTimeKind.Utc).AddTicks(7245), 418, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1919771884" },
                    { 406, 4, 3, 3, "Molestias Nostrum Dolore", new DateTime(2019, 2, 5, 2, 27, 51, 817, DateTimeKind.Utc).AddTicks(7200), 406, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1073934088" },
                    { 292, 6, 3, 3, "Accusantium Consequatur Sit", new DateTime(2019, 2, 9, 23, 7, 44, 895, DateTimeKind.Utc).AddTicks(605), 292, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1891813589" },
                    { 284, 3, 2, 3, "Molestiae Dolor Laudantium", new DateTime(2019, 2, 9, 22, 32, 27, 389, DateTimeKind.Utc).AddTicks(3005), 284, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1253145177" },
                    { 287, 1, 3, 3, "Blanditiis", new DateTime(2019, 2, 13, 4, 9, 23, 951, DateTimeKind.Utc).AddTicks(5311), 287, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1527657234" },
                    { 152, 6, 3, 3, "Consequuntur Asperiores", new DateTime(2019, 2, 7, 6, 26, 32, 774, DateTimeKind.Utc).AddTicks(8825), 152, "https://loremflickr.com/320/240/casino,casinogame/any?lock=746818479" },
                    { 149, 3, 2, 3, "Rerum", new DateTime(2019, 2, 7, 12, 58, 56, 186, DateTimeKind.Utc).AddTicks(5045), 149, "https://loremflickr.com/320/240/casino,casinogame/any?lock=680206375" },
                    { 148, 2, 2, 3, "Provident", new DateTime(2019, 2, 9, 16, 26, 7, 613, DateTimeKind.Utc).AddTicks(1840), 148, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1659778729" },
                    { 145, 6, 3, 3, "Et", new DateTime(2019, 2, 8, 14, 50, 17, 757, DateTimeKind.Utc).AddTicks(2875), 145, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1659549599" },
                    { 140, 3, 3, 3, "Adipisci Laudantium Perspiciatis", new DateTime(2019, 2, 5, 16, 23, 26, 954, DateTimeKind.Utc).AddTicks(5088), 140, "https://loremflickr.com/320/240/casino,casinogame/any?lock=302730076" },
                    { 136, 1, 2, 3, "Consequatur", new DateTime(2019, 2, 8, 22, 23, 59, 683, DateTimeKind.Utc).AddTicks(2266), 136, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1740155766" },
                    { 133, 5, 1, 3, "Voluptas Ut", new DateTime(2019, 2, 13, 11, 59, 53, 581, DateTimeKind.Utc).AddTicks(5336), 133, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1268228856" },
                    { 127, 2, 1, 3, "Non Veritatis", new DateTime(2019, 2, 11, 15, 47, 44, 360, DateTimeKind.Utc).AddTicks(4247), 127, "https://loremflickr.com/320/240/casino,casinogame/any?lock=434055447" },
                    { 118, 6, 2, 3, "Aut Unde Libero", new DateTime(2019, 2, 6, 19, 31, 39, 633, DateTimeKind.Utc).AddTicks(5160), 118, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1242195891" },
                    { 104, 1, 2, 3, "Rerum Consequatur", new DateTime(2019, 2, 13, 11, 0, 39, 662, DateTimeKind.Utc).AddTicks(9734), 104, "https://loremflickr.com/320/240/casino,casinogame/any?lock=426353112" },
                    { 101, 2, 3, 3, "Iusto Doloribus Molestiae", new DateTime(2019, 2, 10, 16, 13, 50, 13, DateTimeKind.Utc).AddTicks(7152), 101, "https://loremflickr.com/320/240/casino,casinogame/any?lock=999601856" },
                    { 99, 6, 2, 3, "Accusantium Qui", new DateTime(2019, 2, 12, 9, 52, 10, 486, DateTimeKind.Utc).AddTicks(5617), 99, "https://loremflickr.com/320/240/casino,casinogame/any?lock=901963961" },
                    { 98, 6, 2, 3, "Asperiores", new DateTime(2019, 2, 7, 7, 4, 57, 57, DateTimeKind.Utc).AddTicks(7725), 98, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1978071924" },
                    { 95, 5, 1, 3, "Et Voluptas Quibusdam", new DateTime(2019, 2, 9, 7, 27, 34, 840, DateTimeKind.Utc).AddTicks(2093), 95, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2069647097" },
                    { 75, 1, 2, 3, "Molestiae Quia Cupiditate", new DateTime(2019, 2, 8, 21, 9, 17, 822, DateTimeKind.Utc).AddTicks(6308), 75, "https://loremflickr.com/320/240/casino,casinogame/any?lock=909752224" },
                    { 73, 5, 3, 3, "Perspiciatis", new DateTime(2019, 2, 9, 14, 36, 7, 441, DateTimeKind.Utc).AddTicks(7356), 73, "https://loremflickr.com/320/240/casino,casinogame/any?lock=403061476" },
                    { 72, 4, 3, 3, "Facilis Nihil At", new DateTime(2019, 2, 14, 2, 15, 42, 275, DateTimeKind.Utc).AddTicks(9079), 72, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1365626036" },
                    { 71, 3, 1, 3, "Inventore Incidunt Labore", new DateTime(2019, 2, 13, 14, 35, 6, 974, DateTimeKind.Utc).AddTicks(2134), 71, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1739710297" },
                    { 69, 1, 3, 3, "Voluptas", new DateTime(2019, 2, 12, 8, 59, 52, 949, DateTimeKind.Utc).AddTicks(5994), 69, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1665364321" },
                    { 67, 4, 3, 3, "Ut Qui Dolorem", new DateTime(2019, 2, 9, 7, 10, 38, 761, DateTimeKind.Utc).AddTicks(4768), 67, "https://loremflickr.com/320/240/casino,casinogame/any?lock=831540288" },
                    { 66, 1, 1, 3, "Molestias", new DateTime(2019, 2, 11, 3, 19, 52, 258, DateTimeKind.Utc).AddTicks(6585), 66, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1749325305" },
                    { 64, 1, 3, 3, "Illo Velit Doloremque", new DateTime(2019, 2, 6, 23, 34, 40, 994, DateTimeKind.Utc).AddTicks(829), 64, "https://loremflickr.com/320/240/casino,casinogame/any?lock=707925149" },
                    { 59, 6, 2, 3, "Neque", new DateTime(2019, 2, 5, 23, 10, 37, 558, DateTimeKind.Utc).AddTicks(1923), 59, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1327927881" },
                    { 52, 4, 2, 3, "Placeat", new DateTime(2019, 2, 9, 7, 19, 4, 287, DateTimeKind.Utc).AddTicks(9176), 52, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1975864863" },
                    { 45, 3, 1, 3, "Sint", new DateTime(2019, 2, 8, 5, 18, 26, 926, DateTimeKind.Utc).AddTicks(4908), 45, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1872064386" },
                    { 22, 6, 2, 3, "Non Sit Sed", new DateTime(2019, 2, 11, 20, 18, 52, 591, DateTimeKind.Utc).AddTicks(8620), 22, "https://loremflickr.com/320/240/casino,casinogame/any?lock=136865536" },
                    { 21, 1, 2, 3, "In Impedit", new DateTime(2019, 2, 11, 12, 47, 10, 965, DateTimeKind.Utc).AddTicks(7615), 21, "https://loremflickr.com/320/240/casino,casinogame/any?lock=977624104" },
                    { 153, 2, 3, 3, "Et", new DateTime(2019, 2, 10, 9, 14, 2, 422, DateTimeKind.Utc).AddTicks(6137), 153, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1515877080" },
                    { 154, 4, 1, 3, "Alias", new DateTime(2019, 2, 6, 15, 56, 17, 670, DateTimeKind.Utc).AddTicks(4341), 154, "https://loremflickr.com/320/240/casino,casinogame/any?lock=62802668" },
                    { 158, 4, 1, 3, "Laborum", new DateTime(2019, 2, 6, 9, 22, 17, 253, DateTimeKind.Utc).AddTicks(1065), 158, "https://loremflickr.com/320/240/casino,casinogame/any?lock=922468234" },
                    { 161, 5, 3, 3, "In Quisquam", new DateTime(2019, 2, 5, 8, 2, 41, 38, DateTimeKind.Utc).AddTicks(7140), 161, "https://loremflickr.com/320/240/casino,casinogame/any?lock=41357553" },
                    { 30, 6, 1, 4, "Et Et", new DateTime(2019, 2, 10, 2, 7, 4, 53, DateTimeKind.Utc).AddTicks(953), 30, "https://loremflickr.com/320/240/casino,casinogame/any?lock=672446459" },
                    { 276, 6, 1, 3, "Et Ipsum", new DateTime(2019, 2, 7, 15, 8, 29, 679, DateTimeKind.Utc).AddTicks(5367), 276, "https://loremflickr.com/320/240/casino,casinogame/any?lock=847122861" },
                    { 271, 5, 2, 3, "Doloremque", new DateTime(2019, 2, 9, 22, 31, 3, 948, DateTimeKind.Utc).AddTicks(5278), 271, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1018223028" },
                    { 266, 5, 3, 3, "Eaque Aperiam Aut", new DateTime(2019, 2, 11, 9, 29, 5, 790, DateTimeKind.Utc).AddTicks(4138), 266, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1917162317" },
                    { 265, 5, 1, 3, "Velit Natus", new DateTime(2019, 2, 11, 17, 21, 6, 661, DateTimeKind.Utc).AddTicks(5471), 265, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1223027471" },
                    { 261, 4, 2, 3, "In Vitae", new DateTime(2019, 2, 9, 21, 42, 30, 676, DateTimeKind.Utc).AddTicks(4079), 261, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1463456341" },
                    { 251, 4, 2, 3, "Et", new DateTime(2019, 2, 5, 17, 12, 20, 169, DateTimeKind.Utc).AddTicks(9406), 251, "https://loremflickr.com/320/240/casino,casinogame/any?lock=640220379" },
                    { 249, 2, 3, 3, "Non Adipisci", new DateTime(2019, 2, 13, 5, 15, 39, 970, DateTimeKind.Utc).AddTicks(8007), 249, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1234535788" },
                    { 248, 4, 2, 3, "Aut", new DateTime(2019, 2, 6, 7, 17, 53, 794, DateTimeKind.Utc).AddTicks(1804), 248, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1350810744" },
                    { 245, 2, 1, 3, "Distinctio", new DateTime(2019, 2, 12, 8, 15, 8, 434, DateTimeKind.Utc).AddTicks(809), 245, "https://loremflickr.com/320/240/casino,casinogame/any?lock=260127869" },
                    { 240, 4, 2, 3, "Dolorum", new DateTime(2019, 2, 10, 21, 15, 46, 330, DateTimeKind.Utc).AddTicks(2502), 240, "https://loremflickr.com/320/240/casino,casinogame/any?lock=406238218" },
                    { 236, 3, 3, 3, "Qui", new DateTime(2019, 2, 4, 17, 22, 33, 72, DateTimeKind.Utc).AddTicks(8700), 236, "https://loremflickr.com/320/240/casino,casinogame/any?lock=149295009" },
                    { 234, 5, 3, 3, "In", new DateTime(2019, 2, 9, 6, 6, 55, 958, DateTimeKind.Utc).AddTicks(7813), 234, "https://loremflickr.com/320/240/casino,casinogame/any?lock=679152330" },
                    { 289, 5, 1, 3, "Et", new DateTime(2019, 2, 8, 10, 15, 51, 816, DateTimeKind.Utc).AddTicks(3934), 289, "https://loremflickr.com/320/240/casino,casinogame/any?lock=764070970" },
                    { 230, 4, 1, 3, "Dolorem Illum Facilis", new DateTime(2019, 2, 8, 20, 44, 24, 585, DateTimeKind.Utc).AddTicks(9845), 230, "https://loremflickr.com/320/240/casino,casinogame/any?lock=826872304" },
                    { 220, 3, 3, 3, "Voluptatem Omnis Repellat", new DateTime(2019, 2, 5, 2, 51, 5, 690, DateTimeKind.Utc).AddTicks(8264), 220, "https://loremflickr.com/320/240/casino,casinogame/any?lock=574901673" },
                    { 218, 4, 1, 3, "Corporis", new DateTime(2019, 2, 8, 0, 20, 56, 112, DateTimeKind.Utc).AddTicks(1972), 218, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1969580244" },
                    { 214, 6, 3, 3, "Ratione Dicta", new DateTime(2019, 2, 8, 19, 27, 59, 250, DateTimeKind.Utc).AddTicks(740), 214, "https://loremflickr.com/320/240/casino,casinogame/any?lock=442226430" },
                    { 210, 5, 2, 3, "Minima", new DateTime(2019, 2, 7, 8, 46, 3, 207, DateTimeKind.Utc).AddTicks(3690), 210, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1645776233" },
                    { 195, 4, 2, 3, "Et Debitis Dolores", new DateTime(2019, 2, 9, 18, 26, 47, 349, DateTimeKind.Utc).AddTicks(2655), 195, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1125851197" },
                    { 194, 3, 3, 3, "Non Nihil", new DateTime(2019, 2, 7, 5, 54, 4, 343, DateTimeKind.Utc).AddTicks(9147), 194, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2099115548" },
                    { 183, 6, 1, 3, "Eius", new DateTime(2019, 2, 10, 6, 23, 58, 434, DateTimeKind.Utc).AddTicks(6666), 183, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1156708546" },
                    { 181, 3, 3, 3, "Et", new DateTime(2019, 2, 14, 0, 31, 5, 709, DateTimeKind.Utc).AddTicks(4987), 181, "https://loremflickr.com/320/240/casino,casinogame/any?lock=617011528" },
                    { 180, 2, 2, 3, "Quam", new DateTime(2019, 2, 13, 8, 1, 35, 676, DateTimeKind.Utc).AddTicks(6357), 180, "https://loremflickr.com/320/240/casino,casinogame/any?lock=341427040" },
                    { 178, 5, 1, 3, "Deleniti Iure", new DateTime(2019, 2, 11, 23, 42, 25, 734, DateTimeKind.Utc).AddTicks(3250), 178, "https://loremflickr.com/320/240/casino,casinogame/any?lock=706250227" },
                    { 167, 5, 3, 3, "Quibusdam", new DateTime(2019, 2, 14, 13, 42, 30, 578, DateTimeKind.Utc).AddTicks(8199), 167, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1616542681" },
                    { 164, 1, 3, 3, "Optio Cumque", new DateTime(2019, 2, 5, 12, 32, 38, 837, DateTimeKind.Utc).AddTicks(244), 164, "https://loremflickr.com/320/240/casino,casinogame/any?lock=971024333" },
                    { 163, 3, 2, 3, "Sint Dolores", new DateTime(2019, 2, 9, 7, 52, 6, 167, DateTimeKind.Utc).AddTicks(9838), 163, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1018211060" },
                    { 221, 2, 2, 3, "Doloribus Et", new DateTime(2019, 2, 6, 2, 7, 11, 837, DateTimeKind.Utc).AddTicks(6040), 221, "https://loremflickr.com/320/240/casino,casinogame/any?lock=653375350" },
                    { 32, 6, 3, 4, "A Reiciendis", new DateTime(2019, 2, 12, 19, 55, 16, 402, DateTimeKind.Utc).AddTicks(1744), 32, "https://loremflickr.com/320/240/casino,casinogame/any?lock=562257207" },
                    { 40, 5, 3, 4, "Necessitatibus Occaecati Asperiores", new DateTime(2019, 2, 14, 3, 38, 8, 266, DateTimeKind.Utc).AddTicks(6073), 40, "https://loremflickr.com/320/240/casino,casinogame/any?lock=650114141" },
                    { 38, 2, 1, 4, "Quisquam", new DateTime(2019, 2, 10, 7, 43, 58, 930, DateTimeKind.Utc).AddTicks(8322), 38, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1068089705" },
                    { 361, 2, 1, 4, "Est Consequatur Voluptas", new DateTime(2019, 2, 6, 6, 55, 43, 627, DateTimeKind.Utc).AddTicks(8363), 361, "https://loremflickr.com/320/240/casino,casinogame/any?lock=553436072" },
                    { 357, 2, 1, 4, "Autem", new DateTime(2019, 2, 6, 16, 44, 18, 656, DateTimeKind.Utc).AddTicks(5558), 357, "https://loremflickr.com/320/240/casino,casinogame/any?lock=287995246" },
                    { 353, 4, 3, 4, "Nostrum", new DateTime(2019, 2, 8, 2, 5, 16, 991, DateTimeKind.Utc).AddTicks(9104), 353, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1413474413" },
                    { 351, 6, 3, 4, "Dolores Ea", new DateTime(2019, 2, 7, 6, 33, 46, 698, DateTimeKind.Utc).AddTicks(134), 351, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1616644242" },
                    { 350, 4, 2, 4, "Aliquid Ex", new DateTime(2019, 2, 5, 14, 49, 40, 746, DateTimeKind.Utc).AddTicks(4122), 350, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1801393998" },
                    { 347, 4, 1, 4, "Illum Facilis", new DateTime(2019, 2, 13, 17, 41, 33, 161, DateTimeKind.Utc).AddTicks(3567), 347, "https://loremflickr.com/320/240/casino,casinogame/any?lock=453920624" },
                    { 344, 2, 3, 4, "Nulla Dolor Reprehenderit", new DateTime(2019, 2, 12, 16, 47, 52, 885, DateTimeKind.Utc).AddTicks(7852), 344, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1094213985" },
                    { 340, 3, 2, 4, "Corporis", new DateTime(2019, 2, 7, 15, 21, 12, 811, DateTimeKind.Utc).AddTicks(1700), 340, "https://loremflickr.com/320/240/casino,casinogame/any?lock=655403114" },
                    { 339, 2, 3, 4, "Fugiat Numquam Perspiciatis", new DateTime(2019, 2, 7, 18, 29, 26, 142, DateTimeKind.Utc).AddTicks(3926), 339, "https://loremflickr.com/320/240/casino,casinogame/any?lock=592225275" },
                    { 334, 6, 2, 4, "Beatae", new DateTime(2019, 2, 6, 23, 24, 48, 407, DateTimeKind.Utc).AddTicks(175), 334, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1751623471" },
                    { 331, 6, 1, 4, "Quidem Culpa Qui", new DateTime(2019, 2, 12, 5, 44, 39, 958, DateTimeKind.Utc).AddTicks(5053), 331, "https://loremflickr.com/320/240/casino,casinogame/any?lock=969763190" },
                    { 329, 4, 1, 4, "Vitae", new DateTime(2019, 2, 12, 0, 41, 2, 806, DateTimeKind.Utc).AddTicks(370), 329, "https://loremflickr.com/320/240/casino,casinogame/any?lock=405866271" },
                    { 328, 2, 2, 4, "Pariatur", new DateTime(2019, 2, 13, 18, 13, 59, 408, DateTimeKind.Utc).AddTicks(5924), 328, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1137276519" },
                    { 319, 5, 2, 4, "Repellendus Nihil", new DateTime(2019, 2, 13, 10, 14, 36, 419, DateTimeKind.Utc).AddTicks(987), 319, "https://loremflickr.com/320/240/casino,casinogame/any?lock=528153370" },
                    { 314, 5, 3, 4, "Saepe Rem Cupiditate", new DateTime(2019, 2, 7, 15, 0, 29, 574, DateTimeKind.Utc).AddTicks(9661), 314, "https://loremflickr.com/320/240/casino,casinogame/any?lock=596794130" },
                    { 313, 5, 1, 4, "Ut Voluptatum Et", new DateTime(2019, 2, 11, 11, 53, 21, 418, DateTimeKind.Utc).AddTicks(2712), 313, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1923766877" },
                    { 312, 1, 3, 4, "Nemo Quas Deleniti", new DateTime(2019, 2, 6, 10, 30, 1, 451, DateTimeKind.Utc).AddTicks(2186), 312, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1167891772" },
                    { 310, 5, 1, 4, "Ab Unde", new DateTime(2019, 2, 6, 11, 14, 12, 847, DateTimeKind.Utc).AddTicks(83), 310, "https://loremflickr.com/320/240/casino,casinogame/any?lock=471885600" },
                    { 309, 5, 1, 4, "Qui Sit", new DateTime(2019, 2, 7, 10, 6, 29, 125, DateTimeKind.Utc).AddTicks(4457), 309, "https://loremflickr.com/320/240/casino,casinogame/any?lock=773054719" },
                    { 301, 6, 2, 4, "Quod", new DateTime(2019, 2, 5, 10, 50, 34, 432, DateTimeKind.Utc).AddTicks(4426), 301, "https://loremflickr.com/320/240/casino,casinogame/any?lock=861563298" },
                    { 298, 1, 3, 4, "Architecto Et", new DateTime(2019, 2, 7, 7, 16, 1, 776, DateTimeKind.Utc).AddTicks(6784), 298, "https://loremflickr.com/320/240/casino,casinogame/any?lock=921764860" },
                    { 291, 3, 2, 4, "Aut Nemo", new DateTime(2019, 2, 13, 2, 39, 31, 588, DateTimeKind.Utc).AddTicks(5220), 291, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1079113250" },
                    { 285, 4, 1, 4, "Quisquam Dolorem Ipsum", new DateTime(2019, 2, 12, 20, 33, 44, 524, DateTimeKind.Utc).AddTicks(2628), 285, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1432695476" },
                    { 274, 3, 2, 4, "Aut Veritatis Suscipit", new DateTime(2019, 2, 10, 10, 53, 22, 915, DateTimeKind.Utc).AddTicks(1916), 274, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1188713615" },
                    { 273, 4, 2, 4, "Veritatis Rerum", new DateTime(2019, 2, 5, 6, 27, 6, 715, DateTimeKind.Utc).AddTicks(9773), 273, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1734143863" },
                    { 269, 5, 3, 4, "Explicabo", new DateTime(2019, 2, 12, 23, 47, 12, 105, DateTimeKind.Utc).AddTicks(5581), 269, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1088636042" },
                    { 264, 6, 3, 4, "Cum", new DateTime(2019, 2, 7, 13, 22, 47, 932, DateTimeKind.Utc).AddTicks(7364), 264, "https://loremflickr.com/320/240/casino,casinogame/any?lock=483513556" },
                    { 362, 3, 3, 4, "Itaque Aliquam", new DateTime(2019, 2, 5, 1, 9, 21, 295, DateTimeKind.Utc).AddTicks(509), 362, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1917458814" },
                    { 366, 2, 1, 4, "Vitae", new DateTime(2019, 2, 14, 5, 53, 49, 563, DateTimeKind.Utc).AddTicks(2716), 366, "https://loremflickr.com/320/240/casino,casinogame/any?lock=58288489" },
                    { 390, 4, 1, 4, "Iste Repellat", new DateTime(2019, 2, 4, 19, 17, 17, 103, DateTimeKind.Utc).AddTicks(1911), 390, "https://loremflickr.com/320/240/casino,casinogame/any?lock=790267777" },
                    { 392, 4, 1, 4, "Delectus Beatae A", new DateTime(2019, 2, 10, 11, 36, 7, 590, DateTimeKind.Utc).AddTicks(7558), 392, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1645306300" },
                    { 489, 6, 2, 4, "Deleniti Facere Natus", new DateTime(2019, 2, 6, 14, 14, 14, 913, DateTimeKind.Utc).AddTicks(7014), 489, "https://loremflickr.com/320/240/casino,casinogame/any?lock=58336021" },
                    { 487, 1, 3, 4, "Esse Enim", new DateTime(2019, 2, 5, 8, 13, 45, 191, DateTimeKind.Utc).AddTicks(92), 487, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1890211449" },
                    { 479, 3, 1, 4, "Sint Quis", new DateTime(2019, 2, 13, 1, 54, 27, 927, DateTimeKind.Utc).AddTicks(6980), 479, "https://loremflickr.com/320/240/casino,casinogame/any?lock=957546687" },
                    { 475, 3, 1, 4, "Quasi", new DateTime(2019, 2, 14, 11, 14, 52, 873, DateTimeKind.Utc).AddTicks(1720), 475, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2132451541" },
                    { 464, 6, 2, 4, "Aut Omnis", new DateTime(2019, 2, 11, 13, 41, 38, 240, DateTimeKind.Utc).AddTicks(60), 464, "https://loremflickr.com/320/240/casino,casinogame/any?lock=464601884" },
                    { 460, 4, 2, 4, "Molestiae Eos", new DateTime(2019, 2, 5, 9, 5, 5, 31, DateTimeKind.Utc).AddTicks(3279), 460, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1397785672" },
                    { 449, 5, 3, 4, "Necessitatibus Beatae", new DateTime(2019, 2, 6, 1, 10, 32, 784, DateTimeKind.Utc).AddTicks(2871), 449, "https://loremflickr.com/320/240/casino,casinogame/any?lock=658563470" },
                    { 448, 6, 2, 4, "Sunt", new DateTime(2019, 2, 6, 10, 51, 40, 147, DateTimeKind.Utc).AddTicks(7770), 448, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1516663166" },
                    { 447, 6, 3, 4, "Cupiditate Architecto", new DateTime(2019, 2, 11, 12, 47, 47, 875, DateTimeKind.Utc).AddTicks(6241), 447, "https://loremflickr.com/320/240/casino,casinogame/any?lock=666634924" },
                    { 446, 3, 2, 4, "Sed", new DateTime(2019, 2, 6, 18, 30, 8, 351, DateTimeKind.Utc).AddTicks(4228), 446, "https://loremflickr.com/320/240/casino,casinogame/any?lock=256348020" },
                    { 444, 5, 1, 4, "Ipsam Vero Reiciendis", new DateTime(2019, 2, 9, 17, 45, 42, 32, DateTimeKind.Utc).AddTicks(6954), 444, "https://loremflickr.com/320/240/casino,casinogame/any?lock=861716299" },
                    { 443, 6, 3, 4, "Aut", new DateTime(2019, 2, 5, 21, 10, 2, 545, DateTimeKind.Utc).AddTicks(723), 443, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1425453619" },
                    { 441, 5, 3, 4, "Odio Doloribus Qui", new DateTime(2019, 2, 7, 8, 21, 42, 808, DateTimeKind.Utc).AddTicks(2075), 441, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1050290926" },
                    { 256, 2, 2, 4, "Harum Harum", new DateTime(2019, 2, 7, 9, 25, 48, 69, DateTimeKind.Utc).AddTicks(3588), 256, "https://loremflickr.com/320/240/casino,casinogame/any?lock=693613639" },
                    { 439, 1, 3, 4, "Dolor", new DateTime(2019, 2, 8, 20, 27, 28, 499, DateTimeKind.Utc).AddTicks(7210), 439, "https://loremflickr.com/320/240/casino,casinogame/any?lock=855234486" },
                    { 436, 6, 1, 4, "Quia", new DateTime(2019, 2, 4, 20, 13, 59, 824, DateTimeKind.Utc).AddTicks(8757), 436, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1551317284" },
                    { 434, 1, 2, 4, "Quo Voluptatem", new DateTime(2019, 2, 8, 22, 8, 18, 81, DateTimeKind.Utc).AddTicks(488), 434, "https://loremflickr.com/320/240/casino,casinogame/any?lock=574086005" },
                    { 430, 1, 3, 4, "Perferendis", new DateTime(2019, 2, 12, 7, 52, 43, 82, DateTimeKind.Utc).AddTicks(1196), 430, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2092666246" },
                    { 427, 1, 3, 4, "Et", new DateTime(2019, 2, 8, 23, 16, 59, 357, DateTimeKind.Utc).AddTicks(3925), 427, "https://loremflickr.com/320/240/casino,casinogame/any?lock=154934909" },
                    { 423, 3, 3, 4, "Sed Et Sunt", new DateTime(2019, 2, 6, 21, 2, 56, 997, DateTimeKind.Utc).AddTicks(6088), 423, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1998622659" },
                    { 411, 2, 3, 4, "Cupiditate", new DateTime(2019, 2, 6, 17, 57, 4, 841, DateTimeKind.Utc).AddTicks(5739), 411, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1297176798" },
                    { 410, 1, 3, 4, "Molestias", new DateTime(2019, 2, 11, 22, 14, 11, 574, DateTimeKind.Utc).AddTicks(4382), 410, "https://loremflickr.com/320/240/casino,casinogame/any?lock=270150696" },
                    { 407, 2, 3, 4, "Quo", new DateTime(2019, 2, 6, 13, 57, 20, 816, DateTimeKind.Utc).AddTicks(1674), 407, "https://loremflickr.com/320/240/casino,casinogame/any?lock=252971569" },
                    { 404, 5, 3, 4, "Asperiores Quod Eos", new DateTime(2019, 2, 5, 2, 39, 48, 160, DateTimeKind.Utc).AddTicks(2353), 404, "https://loremflickr.com/320/240/casino,casinogame/any?lock=759700781" },
                    { 401, 1, 3, 4, "Tempora", new DateTime(2019, 2, 13, 19, 43, 30, 784, DateTimeKind.Utc).AddTicks(4000), 401, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1345791689" },
                    { 398, 4, 2, 4, "Ducimus Illum", new DateTime(2019, 2, 10, 13, 18, 40, 979, DateTimeKind.Utc).AddTicks(2572), 398, "https://loremflickr.com/320/240/casino,casinogame/any?lock=112194095" },
                    { 397, 6, 1, 4, "Quasi", new DateTime(2019, 2, 6, 23, 7, 19, 895, DateTimeKind.Utc).AddTicks(6443), 397, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1076310446" },
                    { 394, 2, 3, 4, "Ex Voluptas A", new DateTime(2019, 2, 6, 12, 7, 39, 621, DateTimeKind.Utc).AddTicks(574), 394, "https://loremflickr.com/320/240/casino,casinogame/any?lock=994194383" },
                    { 438, 4, 1, 4, "Sit Ut", new DateTime(2019, 2, 8, 7, 19, 23, 535, DateTimeKind.Utc).AddTicks(4918), 438, "https://loremflickr.com/320/240/casino,casinogame/any?lock=391747084" },
                    { 35, 4, 1, 4, "Qui Similique Velit", new DateTime(2019, 2, 4, 15, 44, 42, 396, DateTimeKind.Utc).AddTicks(6704), 35, "https://loremflickr.com/320/240/casino,casinogame/any?lock=716424156" },
                    { 253, 3, 2, 4, "Hic", new DateTime(2019, 2, 14, 4, 33, 56, 528, DateTimeKind.Utc).AddTicks(718), 253, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1368064366" },
                    { 229, 6, 1, 4, "Maiores", new DateTime(2019, 2, 9, 5, 43, 58, 211, DateTimeKind.Utc).AddTicks(2440), 229, "https://loremflickr.com/320/240/casino,casinogame/any?lock=280133882" },
                    { 110, 2, 1, 4, "Dolor", new DateTime(2019, 2, 5, 18, 39, 9, 1, DateTimeKind.Utc).AddTicks(737), 110, "https://loremflickr.com/320/240/casino,casinogame/any?lock=565203253" },
                    { 109, 6, 2, 4, "Voluptate Debitis", new DateTime(2019, 2, 7, 15, 55, 38, 38, DateTimeKind.Utc).AddTicks(6301), 109, "https://loremflickr.com/320/240/casino,casinogame/any?lock=203981986" },
                    { 108, 3, 1, 4, "Vel", new DateTime(2019, 2, 8, 11, 25, 18, 426, DateTimeKind.Utc).AddTicks(8510), 108, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1724913576" },
                    { 106, 1, 1, 4, "Quam Maiores Asperiores", new DateTime(2019, 2, 11, 21, 16, 9, 133, DateTimeKind.Utc).AddTicks(7482), 106, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2014996201" },
                    { 103, 2, 2, 4, "Necessitatibus Facere", new DateTime(2019, 2, 9, 1, 40, 10, 487, DateTimeKind.Utc).AddTicks(15), 103, "https://loremflickr.com/320/240/casino,casinogame/any?lock=681213806" },
                    { 100, 2, 1, 4, "Similique", new DateTime(2019, 2, 14, 11, 49, 1, 806, DateTimeKind.Utc).AddTicks(6560), 100, "https://loremflickr.com/320/240/casino,casinogame/any?lock=189449151" },
                    { 97, 3, 1, 4, "Architecto", new DateTime(2019, 2, 12, 17, 13, 12, 965, DateTimeKind.Utc).AddTicks(4425), 97, "https://loremflickr.com/320/240/casino,casinogame/any?lock=405390407" },
                    { 93, 2, 1, 4, "Autem", new DateTime(2019, 2, 11, 22, 34, 1, 132, DateTimeKind.Utc).AddTicks(4770), 93, "https://loremflickr.com/320/240/casino,casinogame/any?lock=459251615" },
                    { 90, 3, 1, 4, "Totam Blanditiis Iusto", new DateTime(2019, 2, 7, 0, 57, 36, 163, DateTimeKind.Utc).AddTicks(7886), 90, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1283584916" },
                    { 87, 5, 1, 4, "Sed Ut", new DateTime(2019, 2, 8, 9, 26, 23, 851, DateTimeKind.Utc).AddTicks(682), 87, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1956506101" },
                    { 84, 5, 3, 4, "Rerum Alias", new DateTime(2019, 2, 12, 12, 5, 13, 996, DateTimeKind.Utc).AddTicks(5874), 84, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1692251172" },
                    { 83, 6, 2, 4, "Et Sunt Nostrum", new DateTime(2019, 2, 12, 2, 50, 26, 976, DateTimeKind.Utc).AddTicks(1530), 83, "https://loremflickr.com/320/240/casino,casinogame/any?lock=185155986" },
                    { 82, 4, 3, 4, "Delectus Qui Nihil", new DateTime(2019, 2, 7, 21, 30, 58, 612, DateTimeKind.Utc).AddTicks(5178), 82, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1386754695" },
                    { 78, 5, 2, 4, "Illo Aspernatur Est", new DateTime(2019, 2, 12, 22, 10, 19, 285, DateTimeKind.Utc).AddTicks(675), 78, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1850784831" },
                    { 76, 6, 2, 4, "Sint Neque", new DateTime(2019, 2, 12, 21, 31, 30, 78, DateTimeKind.Utc).AddTicks(2135), 76, "https://loremflickr.com/320/240/casino,casinogame/any?lock=886667995" },
                    { 70, 4, 3, 4, "Explicabo", new DateTime(2019, 2, 12, 2, 0, 15, 916, DateTimeKind.Utc).AddTicks(9717), 70, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2063100761" },
                    { 68, 2, 3, 4, "Culpa", new DateTime(2019, 2, 13, 18, 34, 13, 905, DateTimeKind.Utc).AddTicks(4366), 68, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2075838219" },
                    { 60, 4, 3, 4, "Laboriosam Nihil Omnis", new DateTime(2019, 2, 11, 21, 25, 37, 687, DateTimeKind.Utc).AddTicks(2349), 60, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1209381225" },
                    { 58, 3, 1, 4, "Quia Et Aut", new DateTime(2019, 2, 9, 18, 21, 9, 174, DateTimeKind.Utc).AddTicks(8983), 58, "https://loremflickr.com/320/240/casino,casinogame/any?lock=968136543" },
                    { 55, 2, 3, 4, "Dolorem Voluptatem Omnis", new DateTime(2019, 2, 10, 15, 7, 14, 819, DateTimeKind.Utc).AddTicks(6726), 55, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1526689346" },
                    { 54, 2, 1, 4, "Qui Quae Illo", new DateTime(2019, 2, 8, 7, 16, 43, 875, DateTimeKind.Utc).AddTicks(3761), 54, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1462196146" },
                    { 53, 2, 3, 4, "Dolorem In", new DateTime(2019, 2, 6, 15, 13, 51, 175, DateTimeKind.Utc).AddTicks(1079), 53, "https://loremflickr.com/320/240/casino,casinogame/any?lock=180590018" },
                    { 50, 1, 1, 4, "Fugit Minus Laboriosam", new DateTime(2019, 2, 12, 5, 59, 4, 525, DateTimeKind.Utc).AddTicks(1262), 50, "https://loremflickr.com/320/240/casino,casinogame/any?lock=4762408" },
                    { 49, 6, 3, 4, "Omnis Omnis", new DateTime(2019, 2, 8, 16, 8, 59, 738, DateTimeKind.Utc).AddTicks(7542), 49, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2069758677" },
                    { 48, 1, 3, 4, "Ducimus", new DateTime(2019, 2, 14, 2, 33, 33, 573, DateTimeKind.Utc).AddTicks(5215), 48, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1038629395" },
                    { 44, 6, 2, 4, "Nobis Nobis Aliquid", new DateTime(2019, 2, 10, 1, 48, 27, 905, DateTimeKind.Utc).AddTicks(5364), 44, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1601253700" },
                    { 20, 5, 3, 3, "Dolorem", new DateTime(2019, 2, 11, 18, 30, 52, 14, DateTimeKind.Utc).AddTicks(2468), 20, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1992212597" },
                    { 111, 1, 1, 4, "Nesciunt Et Cumque", new DateTime(2019, 2, 14, 4, 55, 49, 167, DateTimeKind.Utc).AddTicks(4938), 111, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1271791060" },
                    { 115, 2, 1, 4, "Nobis Voluptatem Molestiae", new DateTime(2019, 2, 12, 8, 35, 31, 747, DateTimeKind.Utc).AddTicks(9598), 115, "https://loremflickr.com/320/240/casino,casinogame/any?lock=960136261" },
                    { 125, 3, 3, 4, "Dolores", new DateTime(2019, 2, 12, 22, 3, 40, 229, DateTimeKind.Utc).AddTicks(3436), 125, "https://loremflickr.com/320/240/casino,casinogame/any?lock=775667911" },
                    { 128, 5, 3, 4, "Explicabo", new DateTime(2019, 2, 5, 20, 49, 19, 454, DateTimeKind.Utc).AddTicks(8849), 128, "https://loremflickr.com/320/240/casino,casinogame/any?lock=765338096" },
                    { 224, 3, 3, 4, "Pariatur", new DateTime(2019, 2, 13, 14, 47, 50, 415, DateTimeKind.Utc).AddTicks(3218), 224, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1892195661" },
                    { 216, 4, 3, 4, "Cum", new DateTime(2019, 2, 13, 17, 9, 53, 193, DateTimeKind.Utc).AddTicks(4374), 216, "https://loremflickr.com/320/240/casino,casinogame/any?lock=335755287" },
                    { 212, 3, 1, 4, "Ut Commodi", new DateTime(2019, 2, 4, 23, 9, 30, 323, DateTimeKind.Utc).AddTicks(7458), 212, "https://loremflickr.com/320/240/casino,casinogame/any?lock=69870971" },
                    { 209, 6, 1, 4, "Perspiciatis Voluptatem", new DateTime(2019, 2, 7, 6, 14, 29, 399, DateTimeKind.Utc).AddTicks(5147), 209, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2043265429" },
                    { 208, 5, 3, 4, "Atque Laboriosam Voluptas", new DateTime(2019, 2, 9, 8, 13, 8, 301, DateTimeKind.Utc).AddTicks(7980), 208, "https://loremflickr.com/320/240/casino,casinogame/any?lock=30735469" },
                    { 203, 6, 1, 4, "Fugit", new DateTime(2019, 2, 4, 22, 42, 24, 59, DateTimeKind.Utc).AddTicks(3313), 203, "https://loremflickr.com/320/240/casino,casinogame/any?lock=610261432" },
                    { 197, 6, 3, 4, "Eum Voluptatibus", new DateTime(2019, 2, 9, 19, 19, 14, 641, DateTimeKind.Utc).AddTicks(28), 197, "https://loremflickr.com/320/240/casino,casinogame/any?lock=592464041" },
                    { 196, 6, 2, 4, "Placeat", new DateTime(2019, 2, 7, 23, 7, 16, 454, DateTimeKind.Utc).AddTicks(3013), 196, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1747720921" },
                    { 192, 5, 1, 4, "Est Consequatur Ex", new DateTime(2019, 2, 6, 11, 8, 21, 278, DateTimeKind.Utc).AddTicks(3273), 192, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1785226518" },
                    { 191, 3, 3, 4, "Eligendi", new DateTime(2019, 2, 4, 17, 26, 23, 666, DateTimeKind.Utc).AddTicks(4224), 191, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1653401345" },
                    { 186, 1, 3, 4, "Assumenda", new DateTime(2019, 2, 9, 21, 46, 23, 188, DateTimeKind.Utc).AddTicks(5090), 186, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1796070137" },
                    { 175, 2, 2, 4, "Officia", new DateTime(2019, 2, 7, 23, 28, 51, 955, DateTimeKind.Utc).AddTicks(1017), 175, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1928228082" },
                    { 174, 4, 2, 4, "Modi Laboriosam", new DateTime(2019, 2, 12, 22, 38, 3, 776, DateTimeKind.Utc).AddTicks(2922), 174, "https://loremflickr.com/320/240/casino,casinogame/any?lock=371928286" },
                    { 242, 2, 1, 4, "Enim", new DateTime(2019, 2, 9, 20, 7, 43, 325, DateTimeKind.Utc).AddTicks(1290), 242, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1354862471" },
                    { 173, 5, 1, 4, "Possimus Esse Nulla", new DateTime(2019, 2, 9, 1, 11, 34, 445, DateTimeKind.Utc).AddTicks(2227), 173, "https://loremflickr.com/320/240/casino,casinogame/any?lock=110065074" },
                    { 171, 6, 2, 4, "Molestiae Odio", new DateTime(2019, 2, 7, 18, 41, 49, 495, DateTimeKind.Utc).AddTicks(6821), 171, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1858128837" },
                    { 166, 4, 1, 4, "Dolore", new DateTime(2019, 2, 10, 1, 42, 22, 3, DateTimeKind.Utc).AddTicks(1788), 166, "https://loremflickr.com/320/240/casino,casinogame/any?lock=142276721" },
                    { 165, 6, 2, 4, "Nam", new DateTime(2019, 2, 7, 15, 35, 23, 684, DateTimeKind.Utc).AddTicks(2579), 165, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1601827180" },
                    { 160, 6, 1, 4, "Maxime", new DateTime(2019, 2, 12, 17, 32, 41, 553, DateTimeKind.Utc).AddTicks(7237), 160, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2037178861" },
                    { 157, 5, 3, 4, "Nihil", new DateTime(2019, 2, 4, 16, 22, 5, 790, DateTimeKind.Utc).AddTicks(9629), 157, "https://loremflickr.com/320/240/casino,casinogame/any?lock=725299080" },
                    { 151, 5, 2, 4, "Consectetur", new DateTime(2019, 2, 6, 5, 3, 57, 585, DateTimeKind.Utc).AddTicks(3782), 151, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1983727713" },
                    { 146, 3, 2, 4, "Optio Deserunt", new DateTime(2019, 2, 14, 3, 33, 8, 109, DateTimeKind.Utc).AddTicks(1376), 146, "https://loremflickr.com/320/240/casino,casinogame/any?lock=105863695" },
                    { 144, 1, 2, 4, "Quibusdam Non Amet", new DateTime(2019, 2, 7, 12, 33, 6, 668, DateTimeKind.Utc).AddTicks(8341), 144, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2059405473" },
                    { 143, 2, 2, 4, "Quae", new DateTime(2019, 2, 12, 15, 35, 21, 379, DateTimeKind.Utc).AddTicks(3891), 143, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2109875283" },
                    { 142, 5, 2, 4, "Nihil Non", new DateTime(2019, 2, 6, 6, 54, 4, 119, DateTimeKind.Utc).AddTicks(2077), 142, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1333310983" },
                    { 141, 5, 1, 4, "In Maiores", new DateTime(2019, 2, 12, 4, 55, 50, 854, DateTimeKind.Utc).AddTicks(280), 141, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2007784256" },
                    { 134, 5, 1, 4, "In Voluptatem Soluta", new DateTime(2019, 2, 7, 23, 41, 10, 882, DateTimeKind.Utc).AddTicks(2815), 134, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1871665773" },
                    { 129, 5, 1, 4, "Occaecati Consequatur Maxime", new DateTime(2019, 2, 5, 7, 58, 31, 25, DateTimeKind.Utc).AddTicks(7888), 129, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1047374684" },
                    { 172, 2, 3, 4, "Cumque", new DateTime(2019, 2, 13, 9, 49, 2, 255, DateTimeKind.Utc).AddTicks(1820), 172, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1181062346" },
                    { 19, 5, 2, 3, "Consequatur", new DateTime(2019, 2, 12, 0, 9, 26, 19, DateTimeKind.Utc).AddTicks(1227), 19, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1083005121" },
                    { 14, 5, 2, 3, "Voluptatibus Omnis Earum", new DateTime(2019, 2, 11, 21, 41, 0, 34, DateTimeKind.Utc).AddTicks(8103), 14, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1656703320" },
                    { 490, 2, 3, 4, "Asperiores Ullam Voluptatem", new DateTime(2019, 2, 7, 1, 34, 25, 583, DateTimeKind.Utc).AddTicks(5434), 490, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1296183279" },
                    { 363, 2, 1, 1, "Labore Pariatur", new DateTime(2019, 2, 8, 16, 55, 29, 380, DateTimeKind.Utc).AddTicks(6154), 363, "https://loremflickr.com/320/240/casino,casinogame/any?lock=796170768" },
                    { 360, 1, 3, 1, "Aut", new DateTime(2019, 2, 5, 21, 24, 1, 337, DateTimeKind.Utc).AddTicks(9126), 360, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1928203646" },
                    { 358, 3, 1, 1, "Possimus Quibusdam", new DateTime(2019, 2, 8, 2, 49, 44, 603, DateTimeKind.Utc).AddTicks(2964), 358, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2120544232" },
                    { 345, 1, 1, 1, "Ut Perspiciatis Rerum", new DateTime(2019, 2, 11, 7, 30, 16, 489, DateTimeKind.Utc).AddTicks(7737), 345, "https://loremflickr.com/320/240/casino,casinogame/any?lock=85173242" },
                    { 343, 5, 2, 1, "Ipsum Ea Atque", new DateTime(2019, 2, 5, 9, 18, 55, 714, DateTimeKind.Utc).AddTicks(9988), 343, "https://loremflickr.com/320/240/casino,casinogame/any?lock=319805808" },
                    { 336, 2, 3, 1, "Voluptatem", new DateTime(2019, 2, 7, 13, 0, 36, 75, DateTimeKind.Utc).AddTicks(1686), 336, "https://loremflickr.com/320/240/casino,casinogame/any?lock=32098259" },
                    { 335, 5, 3, 1, "Ut Nemo Vero", new DateTime(2019, 2, 12, 21, 24, 49, 597, DateTimeKind.Utc).AddTicks(4636), 335, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1385614182" },
                    { 333, 4, 1, 1, "Repellat", new DateTime(2019, 2, 14, 12, 25, 22, 545, DateTimeKind.Utc).AddTicks(3157), 333, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1909293614" }
                });

            migrationBuilder.InsertData(
                table: "tGames",
                columns: new[] { "Id", "GameCategoryId", "GamePlatformId", "GameProviderId", "Name", "ReleaseDate", "SortOrder", "ThumbnailUrl" },
                values: new object[,]
                {
                    { 332, 2, 1, 1, "Voluptas Ducimus", new DateTime(2019, 2, 12, 10, 1, 35, 895, DateTimeKind.Utc).AddTicks(8211), 332, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1016562714" },
                    { 323, 2, 2, 1, "In", new DateTime(2019, 2, 9, 1, 8, 45, 693, DateTimeKind.Utc).AddTicks(241), 323, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1062298966" },
                    { 317, 1, 3, 1, "Eius", new DateTime(2019, 2, 8, 6, 41, 2, 597, DateTimeKind.Utc).AddTicks(8155), 317, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1774216878" },
                    { 315, 1, 3, 1, "Et Vero", new DateTime(2019, 2, 5, 20, 5, 3, 801, DateTimeKind.Utc).AddTicks(5068), 315, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1767921086" },
                    { 302, 1, 2, 1, "Quia Et", new DateTime(2019, 2, 5, 21, 30, 19, 532, DateTimeKind.Utc).AddTicks(6550), 302, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1851618983" },
                    { 300, 1, 3, 1, "Et Cumque Ex", new DateTime(2019, 2, 7, 7, 4, 21, 813, DateTimeKind.Utc).AddTicks(4745), 300, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1427344719" },
                    { 293, 3, 2, 1, "Debitis Sint", new DateTime(2019, 2, 12, 11, 12, 4, 291, DateTimeKind.Utc).AddTicks(624), 293, "https://loremflickr.com/320/240/casino,casinogame/any?lock=888833189" },
                    { 288, 6, 2, 1, "Quia Incidunt Recusandae", new DateTime(2019, 2, 5, 21, 38, 5, 155, DateTimeKind.Utc).AddTicks(4788), 288, "https://loremflickr.com/320/240/casino,casinogame/any?lock=269424735" },
                    { 281, 3, 3, 1, "Ab Suscipit Doloremque", new DateTime(2019, 2, 8, 16, 53, 55, 375, DateTimeKind.Utc).AddTicks(4071), 281, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1100488781" },
                    { 278, 4, 3, 1, "Tempore", new DateTime(2019, 2, 5, 17, 48, 52, 22, DateTimeKind.Utc).AddTicks(2113), 278, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2039115691" },
                    { 275, 2, 3, 1, "Est Possimus Tempore", new DateTime(2019, 2, 12, 3, 50, 57, 44, DateTimeKind.Utc).AddTicks(513), 275, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1335316090" },
                    { 272, 4, 3, 1, "Magni Dolorem", new DateTime(2019, 2, 6, 23, 26, 40, 339, DateTimeKind.Utc).AddTicks(2475), 272, "https://loremflickr.com/320/240/casino,casinogame/any?lock=712469627" },
                    { 267, 6, 3, 1, "Et Minus Atque", new DateTime(2019, 2, 12, 23, 15, 36, 577, DateTimeKind.Utc).AddTicks(3019), 267, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1361927119" },
                    { 262, 4, 3, 1, "Magnam", new DateTime(2019, 2, 9, 19, 27, 50, 711, DateTimeKind.Utc).AddTicks(7851), 262, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1127488831" },
                    { 259, 1, 2, 1, "Cumque Error", new DateTime(2019, 2, 8, 9, 26, 38, 730, DateTimeKind.Utc).AddTicks(6594), 259, "https://loremflickr.com/320/240/casino,casinogame/any?lock=721085726" },
                    { 258, 1, 1, 1, "Earum Et Ea", new DateTime(2019, 2, 12, 13, 33, 11, 90, DateTimeKind.Utc).AddTicks(6613), 258, "https://loremflickr.com/320/240/casino,casinogame/any?lock=505151585" },
                    { 257, 6, 1, 1, "Voluptas Officiis Atque", new DateTime(2019, 2, 6, 3, 40, 48, 548, DateTimeKind.Utc).AddTicks(4419), 257, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1251815899" },
                    { 255, 6, 1, 1, "Aut Est", new DateTime(2019, 2, 10, 6, 48, 32, 355, DateTimeKind.Utc).AddTicks(4530), 255, "https://loremflickr.com/320/240/casino,casinogame/any?lock=125886719" },
                    { 250, 3, 2, 1, "Eveniet Quasi", new DateTime(2019, 2, 6, 13, 39, 37, 426, DateTimeKind.Utc).AddTicks(9663), 250, "https://loremflickr.com/320/240/casino,casinogame/any?lock=249877505" },
                    { 370, 6, 3, 1, "Ipsum Assumenda", new DateTime(2019, 2, 9, 1, 11, 15, 579, DateTimeKind.Utc).AddTicks(55), 370, "https://loremflickr.com/320/240/casino,casinogame/any?lock=737882279" },
                    { 247, 4, 1, 1, "Quod Nisi", new DateTime(2019, 2, 5, 10, 43, 35, 919, DateTimeKind.Utc).AddTicks(308), 247, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1278257344" },
                    { 374, 4, 3, 1, "Neque Dolorum Molestias", new DateTime(2019, 2, 14, 2, 15, 34, 996, DateTimeKind.Utc).AddTicks(2380), 374, "https://loremflickr.com/320/240/casino,casinogame/any?lock=276237598" },
                    { 378, 2, 3, 1, "Similique Dolores Quasi", new DateTime(2019, 2, 12, 6, 22, 59, 460, DateTimeKind.Utc).AddTicks(6692), 378, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1163609604" },
                    { 478, 4, 3, 1, "Doloribus Sed", new DateTime(2019, 2, 10, 23, 30, 18, 149, DateTimeKind.Utc).AddTicks(3807), 478, "https://loremflickr.com/320/240/casino,casinogame/any?lock=231548405" },
                    { 473, 1, 3, 1, "Quis Porro Distinctio", new DateTime(2019, 2, 9, 23, 18, 31, 15, DateTimeKind.Utc).AddTicks(2679), 473, "https://loremflickr.com/320/240/casino,casinogame/any?lock=526686624" },
                    { 472, 6, 2, 1, "Aut", new DateTime(2019, 2, 13, 8, 40, 19, 808, DateTimeKind.Utc).AddTicks(597), 472, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1286962737" },
                    { 468, 6, 1, 1, "In", new DateTime(2019, 2, 8, 20, 5, 16, 420, DateTimeKind.Utc).AddTicks(1926), 468, "https://loremflickr.com/320/240/casino,casinogame/any?lock=378779135" },
                    { 467, 5, 1, 1, "Beatae", new DateTime(2019, 2, 8, 3, 59, 53, 936, DateTimeKind.Utc).AddTicks(9118), 467, "https://loremflickr.com/320/240/casino,casinogame/any?lock=757208881" },
                    { 463, 1, 3, 1, "Consectetur Esse", new DateTime(2019, 2, 9, 6, 0, 20, 476, DateTimeKind.Utc).AddTicks(3752), 463, "https://loremflickr.com/320/240/casino,casinogame/any?lock=714476200" },
                    { 461, 6, 2, 1, "Corporis", new DateTime(2019, 2, 5, 4, 52, 17, 104, DateTimeKind.Utc).AddTicks(7495), 461, "https://loremflickr.com/320/240/casino,casinogame/any?lock=663484392" },
                    { 458, 1, 1, 1, "Nam", new DateTime(2019, 2, 4, 19, 39, 45, 488, DateTimeKind.Utc).AddTicks(9200), 458, "https://loremflickr.com/320/240/casino,casinogame/any?lock=420173525" },
                    { 457, 3, 1, 1, "Totam Non Ducimus", new DateTime(2019, 2, 14, 10, 23, 32, 488, DateTimeKind.Utc).AddTicks(6667), 457, "https://loremflickr.com/320/240/casino,casinogame/any?lock=862071402" },
                    { 456, 6, 3, 1, "Numquam Laborum", new DateTime(2019, 2, 6, 1, 49, 16, 564, DateTimeKind.Utc).AddTicks(9157), 456, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1534764010" },
                    { 455, 2, 1, 1, "Porro Ipsam Non", new DateTime(2019, 2, 11, 13, 14, 43, 775, DateTimeKind.Utc).AddTicks(5700), 455, "https://loremflickr.com/320/240/casino,casinogame/any?lock=959735072" },
                    { 442, 2, 2, 1, "Ea Deleniti", new DateTime(2019, 2, 9, 6, 33, 2, 86, DateTimeKind.Utc).AddTicks(2244), 442, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1013467653" },
                    { 433, 5, 2, 1, "Quo Nulla Accusantium", new DateTime(2019, 2, 11, 6, 20, 22, 919, DateTimeKind.Utc).AddTicks(7477), 433, "https://loremflickr.com/320/240/casino,casinogame/any?lock=992292325" },
                    { 426, 3, 2, 1, "Assumenda Beatae", new DateTime(2019, 2, 7, 10, 37, 23, 882, DateTimeKind.Utc).AddTicks(4633), 426, "https://loremflickr.com/320/240/casino,casinogame/any?lock=67611519" },
                    { 421, 6, 2, 1, "Beatae Ad Itaque", new DateTime(2019, 2, 12, 9, 55, 53, 381, DateTimeKind.Utc).AddTicks(5607), 421, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2061641227" },
                    { 420, 6, 3, 1, "Quia Corporis", new DateTime(2019, 2, 11, 1, 6, 34, 386, DateTimeKind.Utc).AddTicks(9163), 420, "https://loremflickr.com/320/240/casino,casinogame/any?lock=582485565" },
                    { 416, 1, 2, 1, "Occaecati Minima", new DateTime(2019, 2, 9, 1, 26, 19, 222, DateTimeKind.Utc).AddTicks(3109), 416, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1531365220" },
                    { 414, 4, 2, 1, "Ad Explicabo Maiores", new DateTime(2019, 2, 5, 14, 2, 56, 332, DateTimeKind.Utc).AddTicks(6706), 414, "https://loremflickr.com/320/240/casino,casinogame/any?lock=441579855" },
                    { 412, 3, 1, 1, "Quibusdam Consequatur Aspernatur", new DateTime(2019, 2, 9, 16, 14, 50, 917, DateTimeKind.Utc).AddTicks(560), 412, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1991302412" },
                    { 409, 1, 1, 1, "Ratione Tempora Beatae", new DateTime(2019, 2, 13, 16, 35, 2, 993, DateTimeKind.Utc).AddTicks(248), 409, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1829573731" },
                    { 408, 5, 2, 1, "Porro", new DateTime(2019, 2, 12, 15, 8, 3, 92, DateTimeKind.Utc).AddTicks(4235), 408, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1222904624" },
                    { 395, 2, 2, 1, "Voluptatem Alias", new DateTime(2019, 2, 13, 6, 9, 30, 111, DateTimeKind.Utc).AddTicks(8268), 395, "https://loremflickr.com/320/240/casino,casinogame/any?lock=64459643" },
                    { 393, 1, 1, 1, "Dolore", new DateTime(2019, 2, 10, 13, 32, 2, 405, DateTimeKind.Utc).AddTicks(9867), 393, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2132215694" },
                    { 389, 3, 2, 1, "Ut Officiis", new DateTime(2019, 2, 13, 7, 51, 33, 662, DateTimeKind.Utc).AddTicks(1601), 389, "https://loremflickr.com/320/240/casino,casinogame/any?lock=213568828" },
                    { 387, 3, 3, 1, "Incidunt", new DateTime(2019, 2, 7, 21, 13, 59, 656, DateTimeKind.Utc).AddTicks(5896), 387, "https://loremflickr.com/320/240/casino,casinogame/any?lock=103949016" },
                    { 386, 4, 2, 1, "Velit Quis Quos", new DateTime(2019, 2, 4, 17, 15, 53, 707, DateTimeKind.Utc).AddTicks(5482), 386, "https://loremflickr.com/320/240/casino,casinogame/any?lock=913080089" },
                    { 379, 6, 3, 1, "Illo", new DateTime(2019, 2, 9, 0, 21, 13, 609, DateTimeKind.Utc).AddTicks(4210), 379, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1376442827" },
                    { 376, 4, 3, 1, "Qui Eaque", new DateTime(2019, 2, 8, 11, 55, 56, 748, DateTimeKind.Utc).AddTicks(7315), 376, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1250120532" },
                    { 244, 3, 2, 1, "Dolores Velit Enim", new DateTime(2019, 2, 8, 6, 37, 57, 621, DateTimeKind.Utc).AddTicks(1149), 244, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2146252783" },
                    { 243, 1, 2, 1, "Sed Nulla Iste", new DateTime(2019, 2, 7, 6, 47, 36, 106, DateTimeKind.Utc).AddTicks(6877), 243, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1232492049" },
                    { 239, 2, 2, 1, "Nihil", new DateTime(2019, 2, 5, 19, 46, 43, 555, DateTimeKind.Utc).AddTicks(5624), 239, "https://loremflickr.com/320/240/casino,casinogame/any?lock=800630210" },
                    { 88, 6, 2, 1, "Inventore Quam", new DateTime(2019, 2, 5, 8, 1, 22, 310, DateTimeKind.Utc).AddTicks(8870), 88, "https://loremflickr.com/320/240/casino,casinogame/any?lock=154489317" },
                    { 86, 4, 3, 1, "Quia", new DateTime(2019, 2, 6, 3, 24, 8, 207, DateTimeKind.Utc).AddTicks(6939), 86, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2072677909" },
                    { 80, 2, 3, 1, "Ut Ducimus", new DateTime(2019, 2, 13, 22, 13, 17, 524, DateTimeKind.Utc).AddTicks(211), 80, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1441067061" },
                    { 79, 6, 3, 1, "Molestiae Quod Quia", new DateTime(2019, 2, 13, 4, 23, 42, 582, DateTimeKind.Utc).AddTicks(9982), 79, "https://loremflickr.com/320/240/casino,casinogame/any?lock=257156514" },
                    { 77, 2, 2, 1, "Assumenda Delectus", new DateTime(2019, 2, 5, 19, 23, 2, 433, DateTimeKind.Utc).AddTicks(4034), 77, "https://loremflickr.com/320/240/casino,casinogame/any?lock=961215821" },
                    { 74, 1, 1, 1, "Voluptas", new DateTime(2019, 2, 13, 14, 40, 55, 748, DateTimeKind.Utc).AddTicks(8461), 74, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1682773149" },
                    { 63, 2, 1, 1, "Molestiae Quae", new DateTime(2019, 2, 14, 3, 36, 39, 293, DateTimeKind.Utc).AddTicks(1686), 63, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1224261500" },
                    { 62, 5, 2, 1, "Et Eos", new DateTime(2019, 2, 12, 14, 25, 35, 270, DateTimeKind.Utc).AddTicks(428), 62, "https://loremflickr.com/320/240/casino,casinogame/any?lock=607594322" },
                    { 61, 2, 2, 1, "Voluptatem", new DateTime(2019, 2, 14, 1, 31, 54, 350, DateTimeKind.Utc).AddTicks(503), 61, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1397692416" },
                    { 57, 3, 1, 1, "Eius Accusantium Tempore", new DateTime(2019, 2, 10, 18, 30, 16, 984, DateTimeKind.Utc).AddTicks(9583), 57, "https://loremflickr.com/320/240/casino,casinogame/any?lock=589067319" },
                    { 51, 6, 2, 1, "In Consectetur Quas", new DateTime(2019, 2, 5, 4, 0, 7, 246, DateTimeKind.Utc).AddTicks(5602), 51, "https://loremflickr.com/320/240/casino,casinogame/any?lock=257463469" },
                    { 46, 4, 2, 1, "Omnis", new DateTime(2019, 2, 11, 20, 16, 25, 859, DateTimeKind.Utc).AddTicks(3429), 46, "https://loremflickr.com/320/240/casino,casinogame/any?lock=235247845" },
                    { 43, 6, 2, 1, "Non", new DateTime(2019, 2, 7, 10, 50, 11, 58, DateTimeKind.Utc).AddTicks(1962), 43, "https://loremflickr.com/320/240/casino,casinogame/any?lock=214958519" },
                    { 42, 2, 1, 1, "Est", new DateTime(2019, 2, 8, 22, 10, 19, 225, DateTimeKind.Utc).AddTicks(6410), 42, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1795818098" },
                    { 41, 6, 2, 1, "Dicta Est In", new DateTime(2019, 2, 12, 2, 36, 2, 708, DateTimeKind.Utc).AddTicks(6293), 41, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1588624020" },
                    { 37, 2, 2, 1, "Dolorem", new DateTime(2019, 2, 14, 10, 39, 31, 848, DateTimeKind.Utc).AddTicks(6368), 37, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1812207285" },
                    { 36, 5, 1, 1, "Quisquam", new DateTime(2019, 2, 10, 1, 13, 50, 841, DateTimeKind.Utc).AddTicks(7223), 36, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1106181898" },
                    { 34, 3, 3, 1, "Minima Animi", new DateTime(2019, 2, 5, 9, 19, 4, 619, DateTimeKind.Utc).AddTicks(2383), 34, "https://loremflickr.com/320/240/casino,casinogame/any?lock=257656250" },
                    { 31, 1, 3, 1, "Cum Molestiae Assumenda", new DateTime(2019, 2, 8, 2, 37, 49, 92, DateTimeKind.Utc).AddTicks(9422), 31, "https://loremflickr.com/320/240/casino,casinogame/any?lock=733014133" },
                    { 29, 3, 2, 1, "Nobis", new DateTime(2019, 2, 11, 15, 24, 57, 850, DateTimeKind.Utc).AddTicks(2932), 29, "https://loremflickr.com/320/240/casino,casinogame/any?lock=108232747" },
                    { 27, 3, 3, 1, "Libero Aspernatur", new DateTime(2019, 2, 5, 4, 49, 49, 897, DateTimeKind.Utc).AddTicks(7184), 27, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1438395187" },
                    { 25, 2, 3, 1, "Sit Est", new DateTime(2019, 2, 6, 4, 18, 28, 720, DateTimeKind.Utc).AddTicks(3392), 25, "https://loremflickr.com/320/240/casino,casinogame/any?lock=318768276" },
                    { 18, 5, 3, 1, "Officia", new DateTime(2019, 2, 7, 5, 41, 10, 382, DateTimeKind.Utc).AddTicks(973), 18, "https://loremflickr.com/320/240/casino,casinogame/any?lock=794549175" },
                    { 17, 4, 3, 1, "Dolorem Consequatur Aspernatur", new DateTime(2019, 2, 11, 22, 55, 41, 181, DateTimeKind.Utc).AddTicks(7175), 17, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1236108874" },
                    { 15, 2, 3, 1, "Itaque", new DateTime(2019, 2, 11, 4, 49, 46, 920, DateTimeKind.Utc).AddTicks(3069), 15, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1870000432" },
                    { 9, 1, 2, 1, "In Incidunt Natus", new DateTime(2019, 2, 11, 8, 55, 9, 158, DateTimeKind.Utc).AddTicks(2711), 9, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1789468337" },
                    { 4, 3, 3, 1, "Velit", new DateTime(2019, 2, 8, 1, 44, 7, 386, DateTimeKind.Utc).AddTicks(2566), 4, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1381236888" },
                    { 89, 2, 3, 1, "Deserunt", new DateTime(2019, 2, 7, 6, 37, 12, 832, DateTimeKind.Utc).AddTicks(9914), 89, "https://loremflickr.com/320/240/casino,casinogame/any?lock=931699090" },
                    { 91, 3, 2, 1, "Unde Ea Aut", new DateTime(2019, 2, 14, 1, 56, 15, 669, DateTimeKind.Utc).AddTicks(9915), 91, "https://loremflickr.com/320/240/casino,casinogame/any?lock=875491332" },
                    { 92, 1, 1, 1, "Quae Quo", new DateTime(2019, 2, 9, 3, 52, 34, 817, DateTimeKind.Utc).AddTicks(2167), 92, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2089770125" },
                    { 102, 5, 3, 1, "Excepturi Est", new DateTime(2019, 2, 10, 21, 59, 39, 374, DateTimeKind.Utc).AddTicks(1265), 102, "https://loremflickr.com/320/240/casino,casinogame/any?lock=904958976" },
                    { 235, 5, 2, 1, "Aut Quod Tenetur", new DateTime(2019, 2, 9, 15, 29, 1, 181, DateTimeKind.Utc).AddTicks(6277), 235, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1487887219" },
                    { 231, 4, 2, 1, "Eveniet Consequatur", new DateTime(2019, 2, 10, 12, 3, 19, 479, DateTimeKind.Utc).AddTicks(1952), 231, "https://loremflickr.com/320/240/casino,casinogame/any?lock=500633835" },
                    { 227, 3, 2, 1, "Provident Ex", new DateTime(2019, 2, 9, 0, 55, 13, 580, DateTimeKind.Utc).AddTicks(9894), 227, "https://loremflickr.com/320/240/casino,casinogame/any?lock=458885339" },
                    { 225, 6, 2, 1, "Aperiam Nisi Maiores", new DateTime(2019, 2, 8, 15, 34, 45, 684, DateTimeKind.Utc).AddTicks(5076), 225, "https://loremflickr.com/320/240/casino,casinogame/any?lock=538507470" },
                    { 223, 1, 2, 1, "Architecto", new DateTime(2019, 2, 12, 8, 47, 51, 386, DateTimeKind.Utc).AddTicks(4911), 223, "https://loremflickr.com/320/240/casino,casinogame/any?lock=808109049" },
                    { 222, 6, 2, 1, "Aut Rerum Voluptatem", new DateTime(2019, 2, 9, 4, 48, 48, 918, DateTimeKind.Utc).AddTicks(7512), 222, "https://loremflickr.com/320/240/casino,casinogame/any?lock=771737933" },
                    { 207, 2, 1, 1, "Possimus", new DateTime(2019, 2, 8, 16, 12, 55, 621, DateTimeKind.Utc).AddTicks(2964), 207, "https://loremflickr.com/320/240/casino,casinogame/any?lock=636070154" },
                    { 205, 4, 3, 1, "Ea Quas", new DateTime(2019, 2, 12, 22, 12, 42, 296, DateTimeKind.Utc).AddTicks(852), 205, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1452836692" },
                    { 200, 4, 1, 1, "Porro Nobis Non", new DateTime(2019, 2, 13, 11, 49, 58, 877, DateTimeKind.Utc).AddTicks(188), 200, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1114696382" },
                    { 190, 2, 1, 1, "Sit Vel", new DateTime(2019, 2, 9, 17, 49, 56, 225, DateTimeKind.Utc).AddTicks(4442), 190, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1528621624" },
                    { 189, 5, 2, 1, "Quam", new DateTime(2019, 2, 10, 10, 24, 19, 463, DateTimeKind.Utc).AddTicks(3079), 189, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1319389277" },
                    { 188, 3, 2, 1, "Voluptatem Deserunt Quas", new DateTime(2019, 2, 9, 7, 18, 42, 333, DateTimeKind.Utc).AddTicks(819), 188, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1155199906" },
                    { 184, 3, 1, 1, "Molestias Totam", new DateTime(2019, 2, 14, 12, 31, 21, 49, DateTimeKind.Utc).AddTicks(4648), 184, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2118105582" },
                    { 481, 5, 3, 1, "Alias", new DateTime(2019, 2, 7, 14, 32, 3, 715, DateTimeKind.Utc).AddTicks(8534), 481, "https://loremflickr.com/320/240/casino,casinogame/any?lock=810888515" },
                    { 182, 1, 2, 1, "Sit Qui", new DateTime(2019, 2, 12, 23, 33, 43, 862, DateTimeKind.Utc).AddTicks(265), 182, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1159359597" },
                    { 170, 6, 1, 1, "Amet Amet", new DateTime(2019, 2, 6, 18, 51, 13, 295, DateTimeKind.Utc).AddTicks(7533), 170, "https://loremflickr.com/320/240/casino,casinogame/any?lock=427829860" },
                    { 169, 1, 3, 1, "Recusandae Placeat", new DateTime(2019, 2, 11, 0, 6, 33, 269, DateTimeKind.Utc).AddTicks(6593), 169, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1233571348" },
                    { 162, 2, 1, 1, "Accusantium Iste", new DateTime(2019, 2, 9, 5, 35, 37, 377, DateTimeKind.Utc).AddTicks(1394), 162, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2107079218" },
                    { 139, 1, 2, 1, "Totam Blanditiis", new DateTime(2019, 2, 8, 13, 26, 25, 448, DateTimeKind.Utc).AddTicks(6021), 139, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1245906467" },
                    { 132, 1, 1, 1, "Aut", new DateTime(2019, 2, 9, 7, 48, 41, 134, DateTimeKind.Utc).AddTicks(5298), 132, "https://loremflickr.com/320/240/casino,casinogame/any?lock=924713891" },
                    { 131, 3, 1, 1, "Explicabo Exercitationem", new DateTime(2019, 2, 10, 12, 5, 4, 391, DateTimeKind.Utc).AddTicks(3873), 131, "https://loremflickr.com/320/240/casino,casinogame/any?lock=859104460" },
                    { 130, 5, 2, 1, "In Quas Sit", new DateTime(2019, 2, 7, 11, 25, 11, 566, DateTimeKind.Utc).AddTicks(7426), 130, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1101796428" },
                    { 123, 4, 2, 1, "Sit", new DateTime(2019, 2, 11, 21, 16, 17, 178, DateTimeKind.Utc).AddTicks(8537), 123, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1004404343" },
                    { 121, 6, 2, 1, "Nam", new DateTime(2019, 2, 6, 21, 22, 51, 921, DateTimeKind.Utc).AddTicks(6262), 121, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1933063780" },
                    { 120, 5, 2, 1, "Velit Et Est", new DateTime(2019, 2, 10, 1, 5, 45, 251, DateTimeKind.Utc).AddTicks(5808), 120, "https://loremflickr.com/320/240/casino,casinogame/any?lock=297472253" },
                    { 119, 1, 1, 1, "Aut Sequi Laboriosam", new DateTime(2019, 2, 8, 20, 52, 18, 687, DateTimeKind.Utc).AddTicks(3800), 119, "https://loremflickr.com/320/240/casino,casinogame/any?lock=902094482" },
                    { 114, 3, 2, 1, "Et Debitis", new DateTime(2019, 2, 11, 3, 25, 0, 54, DateTimeKind.Utc).AddTicks(4776), 114, "https://loremflickr.com/320/240/casino,casinogame/any?lock=912322704" },
                    { 105, 6, 1, 1, "Veniam", new DateTime(2019, 2, 8, 14, 58, 12, 480, DateTimeKind.Utc).AddTicks(2689), 105, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1563004135" },
                    { 176, 3, 3, 1, "Et Rerum", new DateTime(2019, 2, 9, 1, 49, 51, 465, DateTimeKind.Utc).AddTicks(5715), 176, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1561130635" },
                    { 483, 2, 2, 1, "Ut Commodi Sed", new DateTime(2019, 2, 11, 12, 22, 17, 529, DateTimeKind.Utc).AddTicks(2483), 483, "https://loremflickr.com/320/240/casino,casinogame/any?lock=759104198" },
                    { 488, 5, 2, 1, "Est Voluptatum", new DateTime(2019, 2, 7, 5, 9, 6, 146, DateTimeKind.Utc).AddTicks(5369), 488, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1351230995" },
                    { 493, 3, 2, 1, "Vero Sunt Sed", new DateTime(2019, 2, 12, 14, 52, 43, 749, DateTimeKind.Utc).AddTicks(6472), 493, "https://loremflickr.com/320/240/casino,casinogame/any?lock=153110248" },
                    { 365, 2, 3, 2, "Deserunt", new DateTime(2019, 2, 11, 2, 56, 56, 682, DateTimeKind.Utc).AddTicks(2242), 365, "https://loremflickr.com/320/240/casino,casinogame/any?lock=22021113" },
                    { 364, 3, 1, 2, "Consequatur", new DateTime(2019, 2, 13, 0, 37, 12, 301, DateTimeKind.Utc).AddTicks(8643), 364, "https://loremflickr.com/320/240/casino,casinogame/any?lock=384430534" },
                    { 359, 6, 2, 2, "Officia", new DateTime(2019, 2, 6, 17, 58, 47, 504, DateTimeKind.Utc).AddTicks(9960), 359, "https://loremflickr.com/320/240/casino,casinogame/any?lock=748901945" },
                    { 356, 5, 3, 2, "Deserunt Quo Qui", new DateTime(2019, 2, 11, 2, 47, 51, 130, DateTimeKind.Utc).AddTicks(9214), 356, "https://loremflickr.com/320/240/casino,casinogame/any?lock=993617444" },
                    { 354, 3, 2, 2, "Quia Dolores", new DateTime(2019, 2, 8, 23, 27, 45, 27, DateTimeKind.Utc).AddTicks(6074), 354, "https://loremflickr.com/320/240/casino,casinogame/any?lock=944389603" },
                    { 349, 6, 3, 2, "Sed", new DateTime(2019, 2, 7, 19, 47, 11, 176, DateTimeKind.Utc).AddTicks(837), 349, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2120344435" },
                    { 348, 4, 3, 2, "Quos Ratione Omnis", new DateTime(2019, 2, 9, 21, 53, 36, 951, DateTimeKind.Utc).AddTicks(3132), 348, "https://loremflickr.com/320/240/casino,casinogame/any?lock=830719112" },
                    { 342, 1, 1, 2, "Corrupti", new DateTime(2019, 2, 4, 16, 14, 57, 406, DateTimeKind.Utc).AddTicks(863), 342, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1611303488" },
                    { 338, 5, 2, 2, "Vel", new DateTime(2019, 2, 4, 16, 7, 6, 133, DateTimeKind.Utc).AddTicks(6253), 338, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1021022272" },
                    { 337, 5, 1, 2, "Quia", new DateTime(2019, 2, 11, 8, 6, 11, 286, DateTimeKind.Utc).AddTicks(7299), 337, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1596731383" },
                    { 327, 3, 3, 2, "Ut Voluptas Quaerat", new DateTime(2019, 2, 9, 12, 42, 57, 124, DateTimeKind.Utc).AddTicks(3601), 327, "https://loremflickr.com/320/240/casino,casinogame/any?lock=304255282" },
                    { 322, 3, 1, 2, "Voluptatem", new DateTime(2019, 2, 9, 3, 34, 47, 899, DateTimeKind.Utc).AddTicks(7729), 322, "https://loremflickr.com/320/240/casino,casinogame/any?lock=69518726" },
                    { 321, 2, 1, 2, "Omnis Laborum", new DateTime(2019, 2, 6, 6, 33, 17, 821, DateTimeKind.Utc).AddTicks(6010), 321, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1773865063" },
                    { 318, 3, 3, 2, "Deserunt A Eveniet", new DateTime(2019, 2, 11, 7, 40, 9, 844, DateTimeKind.Utc).AddTicks(6896), 318, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1034207102" },
                    { 307, 5, 2, 2, "Rerum Ratione Ab", new DateTime(2019, 2, 10, 22, 25, 36, 148, DateTimeKind.Utc).AddTicks(5957), 307, "https://loremflickr.com/320/240/casino,casinogame/any?lock=541603087" },
                    { 304, 5, 3, 2, "Quo Hic Voluptatibus", new DateTime(2019, 2, 5, 0, 14, 22, 552, DateTimeKind.Utc).AddTicks(5845), 304, "https://loremflickr.com/320/240/casino,casinogame/any?lock=816686874" },
                    { 299, 5, 1, 2, "Et Rerum", new DateTime(2019, 2, 8, 19, 45, 32, 746, DateTimeKind.Utc).AddTicks(7393), 299, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1733463805" },
                    { 296, 3, 2, 2, "Soluta", new DateTime(2019, 2, 8, 10, 58, 12, 809, DateTimeKind.Utc).AddTicks(2925), 296, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1884906384" },
                    { 295, 1, 3, 2, "A Ratione Blanditiis", new DateTime(2019, 2, 12, 1, 32, 16, 757, DateTimeKind.Utc).AddTicks(7653), 295, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2125480797" },
                    { 294, 4, 2, 2, "Aspernatur Impedit Et", new DateTime(2019, 2, 7, 8, 47, 20, 582, DateTimeKind.Utc).AddTicks(9918), 294, "https://loremflickr.com/320/240/casino,casinogame/any?lock=173137041" },
                    { 290, 2, 1, 2, "Esse", new DateTime(2019, 2, 11, 16, 11, 7, 840, DateTimeKind.Utc).AddTicks(7647), 290, "https://loremflickr.com/320/240/casino,casinogame/any?lock=270792623" },
                    { 286, 2, 3, 2, "Rerum", new DateTime(2019, 2, 9, 8, 38, 43, 182, DateTimeKind.Utc).AddTicks(6551), 286, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2099296654" },
                    { 283, 6, 3, 2, "Harum Ea", new DateTime(2019, 2, 8, 2, 22, 31, 787, DateTimeKind.Utc).AddTicks(7317), 283, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1766688768" },
                    { 282, 1, 2, 2, "Minus Repellendus", new DateTime(2019, 2, 11, 21, 46, 23, 668, DateTimeKind.Utc).AddTicks(512), 282, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1193364908" },
                    { 280, 6, 2, 2, "Rerum Impedit", new DateTime(2019, 2, 8, 23, 9, 55, 356, DateTimeKind.Utc).AddTicks(443), 280, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1371765339" },
                    { 279, 4, 1, 2, "Labore", new DateTime(2019, 2, 9, 9, 41, 55, 398, DateTimeKind.Utc).AddTicks(2153), 279, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1418916661" },
                    { 277, 1, 1, 2, "Libero", new DateTime(2019, 2, 14, 12, 34, 54, 780, DateTimeKind.Utc).AddTicks(4582), 277, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2088306611" },
                    { 368, 6, 2, 2, "Omnis Odio Qui", new DateTime(2019, 2, 12, 18, 42, 50, 873, DateTimeKind.Utc).AddTicks(4469), 368, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1513100791" },
                    { 371, 2, 1, 2, "Quo Est Impedit", new DateTime(2019, 2, 11, 20, 7, 2, 588, DateTimeKind.Utc).AddTicks(8561), 371, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1784821240" },
                    { 383, 1, 2, 2, "Velit", new DateTime(2019, 2, 10, 21, 46, 34, 522, DateTimeKind.Utc).AddTicks(6116), 383, "https://loremflickr.com/320/240/casino,casinogame/any?lock=798700755" },
                    { 384, 3, 2, 2, "Non", new DateTime(2019, 2, 13, 17, 0, 23, 662, DateTimeKind.Utc).AddTicks(9766), 384, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1713598277" },
                    { 13, 2, 1, 3, "Corrupti Eligendi", new DateTime(2019, 2, 8, 1, 53, 42, 97, DateTimeKind.Utc).AddTicks(7345), 13, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1102570023" },
                    { 12, 6, 1, 3, "Quasi Porro Ut", new DateTime(2019, 2, 11, 7, 3, 29, 231, DateTimeKind.Utc).AddTicks(2401), 12, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1472039290" },
                    { 7, 6, 1, 3, "Autem", new DateTime(2019, 2, 14, 2, 48, 55, 317, DateTimeKind.Utc).AddTicks(2248), 7, "https://loremflickr.com/320/240/casino,casinogame/any?lock=254293840" },
                    { 5, 5, 3, 3, "Amet Eos", new DateTime(2019, 2, 14, 2, 47, 4, 151, DateTimeKind.Utc).AddTicks(521), 5, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1124566540" },
                    { 3, 4, 2, 3, "Sunt Ratione", new DateTime(2019, 2, 8, 23, 57, 4, 761, DateTimeKind.Utc).AddTicks(7745), 3, "https://loremflickr.com/320/240/casino,casinogame/any?lock=531648829" },
                    { 2, 6, 1, 3, "Quia Qui", new DateTime(2019, 2, 10, 14, 6, 46, 484, DateTimeKind.Utc).AddTicks(9735), 2, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1934921513" },
                    { 500, 5, 1, 2, "Ea Molestiae", new DateTime(2019, 2, 8, 3, 5, 38, 686, DateTimeKind.Utc).AddTicks(2174), 500, "https://loremflickr.com/320/240/casino,casinogame/any?lock=451302800" },
                    { 499, 1, 1, 2, "Voluptatem Rerum Dolorem", new DateTime(2019, 2, 11, 14, 31, 59, 281, DateTimeKind.Utc).AddTicks(9683), 499, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1470366614" },
                    { 484, 1, 1, 2, "Vel Suscipit", new DateTime(2019, 2, 9, 14, 27, 24, 378, DateTimeKind.Utc).AddTicks(6605), 484, "https://loremflickr.com/320/240/casino,casinogame/any?lock=349624176" },
                    { 482, 2, 2, 2, "Enim", new DateTime(2019, 2, 5, 12, 6, 53, 69, DateTimeKind.Utc).AddTicks(6429), 482, "https://loremflickr.com/320/240/casino,casinogame/any?lock=820301507" },
                    { 477, 1, 1, 2, "Dolorem", new DateTime(2019, 2, 10, 20, 53, 19, 207, DateTimeKind.Utc).AddTicks(4973), 477, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1488027031" },
                    { 476, 4, 2, 2, "Totam Qui", new DateTime(2019, 2, 4, 21, 10, 26, 993, DateTimeKind.Utc).AddTicks(5912), 476, "https://loremflickr.com/320/240/casino,casinogame/any?lock=312712109" },
                    { 474, 2, 2, 2, "Et", new DateTime(2019, 2, 13, 7, 7, 17, 796, DateTimeKind.Utc).AddTicks(5045), 474, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1523670721" },
                    { 270, 1, 3, 2, "Perspiciatis Est Qui", new DateTime(2019, 2, 14, 13, 19, 0, 618, DateTimeKind.Utc).AddTicks(8170), 270, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2072997263" },
                    { 470, 3, 2, 2, "Possimus Non Et", new DateTime(2019, 2, 4, 18, 21, 45, 783, DateTimeKind.Utc).AddTicks(1185), 470, "https://loremflickr.com/320/240/casino,casinogame/any?lock=311259383" },
                    { 462, 1, 3, 2, "Ut Et Tempora", new DateTime(2019, 2, 11, 4, 29, 56, 669, DateTimeKind.Utc).AddTicks(3767), 462, "https://loremflickr.com/320/240/casino,casinogame/any?lock=868426821" },
                    { 459, 5, 1, 2, "Consequatur", new DateTime(2019, 2, 7, 22, 39, 45, 763, DateTimeKind.Utc).AddTicks(8415), 459, "https://loremflickr.com/320/240/casino,casinogame/any?lock=984192515" },
                    { 453, 5, 1, 2, "Dolorum Qui", new DateTime(2019, 2, 4, 23, 0, 27, 182, DateTimeKind.Utc).AddTicks(1834), 453, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1146215448" },
                    { 451, 3, 3, 2, "Odio Quidem", new DateTime(2019, 2, 13, 4, 35, 17, 26, DateTimeKind.Utc).AddTicks(7021), 451, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1451464573" },
                    { 437, 5, 1, 2, "Quod Aliquam Quia", new DateTime(2019, 2, 6, 3, 55, 37, 953, DateTimeKind.Utc).AddTicks(5949), 437, "https://loremflickr.com/320/240/casino,casinogame/any?lock=698992539" },
                    { 425, 6, 1, 2, "Sapiente", new DateTime(2019, 2, 12, 7, 7, 14, 209, DateTimeKind.Utc).AddTicks(7508), 425, "https://loremflickr.com/320/240/casino,casinogame/any?lock=609929422" },
                    { 424, 2, 2, 2, "Beatae", new DateTime(2019, 2, 14, 6, 44, 16, 55, DateTimeKind.Utc).AddTicks(7248), 424, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2051105016" },
                    { 417, 4, 2, 2, "Culpa Consequatur Nesciunt", new DateTime(2019, 2, 9, 13, 7, 3, 506, DateTimeKind.Utc).AddTicks(529), 417, "https://loremflickr.com/320/240/casino,casinogame/any?lock=465010483" },
                    { 415, 2, 1, 2, "Officia Minima", new DateTime(2019, 2, 11, 16, 39, 36, 13, DateTimeKind.Utc).AddTicks(2579), 415, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1628290506" },
                    { 405, 2, 1, 2, "Facere Aut Eius", new DateTime(2019, 2, 6, 16, 37, 55, 805, DateTimeKind.Utc).AddTicks(1428), 405, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2066239027" },
                    { 402, 5, 1, 2, "Accusantium Et", new DateTime(2019, 2, 7, 21, 4, 2, 869, DateTimeKind.Utc).AddTicks(7238), 402, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1102864999" },
                    { 399, 6, 3, 2, "Quos Earum Ipsam", new DateTime(2019, 2, 13, 21, 36, 49, 782, DateTimeKind.Utc).AddTicks(7172), 399, "https://loremflickr.com/320/240/casino,casinogame/any?lock=816151070" },
                    { 391, 2, 2, 2, "Molestias", new DateTime(2019, 2, 9, 7, 52, 56, 872, DateTimeKind.Utc).AddTicks(2677), 391, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1213529938" },
                    { 469, 2, 3, 2, "Repellendus", new DateTime(2019, 2, 4, 16, 24, 24, 679, DateTimeKind.Utc).AddTicks(725), 469, "https://loremflickr.com/320/240/casino,casinogame/any?lock=825861463" },
                    { 16, 1, 2, 3, "Dolor Accusantium", new DateTime(2019, 2, 7, 7, 28, 25, 77, DateTimeKind.Utc).AddTicks(4245), 16, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1942608001" },
                    { 268, 5, 3, 2, "Voluptatibus Doloribus Est", new DateTime(2019, 2, 13, 18, 57, 14, 519, DateTimeKind.Utc).AddTicks(8826), 268, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1724547296" },
                    { 260, 3, 2, 2, "Ut Voluptas Quis", new DateTime(2019, 2, 13, 0, 43, 55, 779, DateTimeKind.Utc).AddTicks(2588), 260, "https://loremflickr.com/320/240/casino,casinogame/any?lock=205541647" },
                    { 138, 2, 2, 2, "Facilis Quas Odio", new DateTime(2019, 2, 9, 21, 59, 41, 835, DateTimeKind.Utc).AddTicks(3028), 138, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1531316974" },
                    { 137, 1, 2, 2, "Architecto", new DateTime(2019, 2, 5, 16, 33, 50, 488, DateTimeKind.Utc).AddTicks(4245), 137, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1272179531" },
                    { 135, 1, 1, 2, "Nesciunt Aliquid", new DateTime(2019, 2, 6, 2, 55, 54, 78, DateTimeKind.Utc).AddTicks(7880), 135, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1395566581" },
                    { 126, 6, 3, 2, "Repellat Alias", new DateTime(2019, 2, 14, 3, 16, 31, 870, DateTimeKind.Utc).AddTicks(9785), 126, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1551532926" },
                    { 124, 2, 1, 2, "Sed", new DateTime(2019, 2, 8, 18, 50, 50, 959, DateTimeKind.Utc).AddTicks(8800), 124, "https://loremflickr.com/320/240/casino,casinogame/any?lock=333468041" },
                    { 122, 3, 3, 2, "Voluptas", new DateTime(2019, 2, 12, 15, 26, 44, 865, DateTimeKind.Utc).AddTicks(356), 122, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2017444084" },
                    { 117, 5, 2, 2, "Nihil", new DateTime(2019, 2, 10, 18, 26, 6, 394, DateTimeKind.Utc).AddTicks(4131), 117, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1681050225" },
                    { 116, 3, 2, 2, "Mollitia", new DateTime(2019, 2, 5, 7, 9, 34, 955, DateTimeKind.Utc).AddTicks(9356), 116, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1587181852" },
                    { 113, 6, 2, 2, "Nihil", new DateTime(2019, 2, 14, 6, 8, 0, 209, DateTimeKind.Utc).AddTicks(3965), 113, "https://loremflickr.com/320/240/casino,casinogame/any?lock=325483914" },
                    { 112, 3, 1, 2, "Laborum Corporis Alias", new DateTime(2019, 2, 9, 0, 48, 28, 116, DateTimeKind.Utc).AddTicks(242), 112, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1281632023" },
                    { 107, 4, 1, 2, "Vero Quo Eaque", new DateTime(2019, 2, 10, 1, 28, 13, 948, DateTimeKind.Utc).AddTicks(6842), 107, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1167269877" },
                    { 96, 3, 3, 2, "Consequatur Modi Mollitia", new DateTime(2019, 2, 14, 8, 58, 45, 666, DateTimeKind.Utc).AddTicks(5069), 96, "https://loremflickr.com/320/240/casino,casinogame/any?lock=579899466" },
                    { 94, 1, 2, 2, "Reprehenderit Eos Veniam", new DateTime(2019, 2, 10, 8, 3, 46, 509, DateTimeKind.Utc).AddTicks(943), 94, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1745097499" },
                    { 85, 5, 3, 2, "Atque", new DateTime(2019, 2, 4, 23, 56, 0, 859, DateTimeKind.Utc).AddTicks(8858), 85, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1408178782" },
                    { 81, 4, 1, 2, "Ad Itaque", new DateTime(2019, 2, 5, 10, 14, 12, 167, DateTimeKind.Utc).AddTicks(447), 81, "https://loremflickr.com/320/240/casino,casinogame/any?lock=160239497" },
                    { 65, 2, 1, 2, "Labore", new DateTime(2019, 2, 6, 15, 45, 32, 207, DateTimeKind.Utc).AddTicks(3133), 65, "https://loremflickr.com/320/240/casino,casinogame/any?lock=953114759" },
                    { 56, 3, 2, 2, "Et Distinctio Suscipit", new DateTime(2019, 2, 8, 19, 1, 8, 934, DateTimeKind.Utc).AddTicks(4290), 56, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1616849745" },
                    { 47, 5, 1, 2, "Pariatur Dicta", new DateTime(2019, 2, 10, 20, 20, 24, 700, DateTimeKind.Utc).AddTicks(8198), 47, "https://loremflickr.com/320/240/casino,casinogame/any?lock=187148726" },
                    { 39, 2, 1, 2, "Molestias Debitis Vero", new DateTime(2019, 2, 8, 17, 30, 59, 852, DateTimeKind.Utc).AddTicks(242), 39, "https://loremflickr.com/320/240/casino,casinogame/any?lock=307697067" },
                    { 33, 6, 1, 2, "Id Non", new DateTime(2019, 2, 6, 14, 58, 48, 970, DateTimeKind.Utc).AddTicks(2747), 33, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1467323658" },
                    { 24, 5, 1, 2, "Sequi", new DateTime(2019, 2, 5, 16, 52, 48, 786, DateTimeKind.Utc).AddTicks(1485), 24, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1788936420" },
                    { 23, 2, 2, 2, "Sint", new DateTime(2019, 2, 5, 3, 27, 59, 778, DateTimeKind.Utc).AddTicks(2748), 23, "https://loremflickr.com/320/240/casino,casinogame/any?lock=999044299" },
                    { 10, 4, 2, 2, "Iusto", new DateTime(2019, 2, 10, 10, 53, 54, 798, DateTimeKind.Utc).AddTicks(430), 10, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1349342538" },
                    { 8, 4, 2, 2, "Ea", new DateTime(2019, 2, 13, 2, 56, 30, 664, DateTimeKind.Utc).AddTicks(8746), 8, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2036595621" },
                    { 498, 6, 3, 1, "Cumque", new DateTime(2019, 2, 13, 22, 37, 24, 344, DateTimeKind.Utc).AddTicks(9217), 498, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1283118420" },
                    { 497, 1, 2, 1, "Asperiores Cum", new DateTime(2019, 2, 5, 15, 54, 21, 180, DateTimeKind.Utc).AddTicks(7961), 497, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1864190367" },
                    { 494, 4, 2, 1, "Omnis Qui Vero", new DateTime(2019, 2, 6, 21, 1, 3, 181, DateTimeKind.Utc).AddTicks(6817), 494, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1205408291" },
                    { 147, 6, 3, 2, "Exercitationem", new DateTime(2019, 2, 14, 5, 38, 40, 164, DateTimeKind.Utc).AddTicks(8507), 147, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1762657527" },
                    { 150, 5, 1, 2, "Ut", new DateTime(2019, 2, 5, 7, 33, 8, 513, DateTimeKind.Utc).AddTicks(9757), 150, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1273354860" },
                    { 155, 6, 1, 2, "Qui Aperiam Saepe", new DateTime(2019, 2, 4, 20, 40, 1, 622, DateTimeKind.Utc).AddTicks(5303), 155, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1064812540" },
                    { 156, 4, 3, 2, "Ut Pariatur Voluptas", new DateTime(2019, 2, 10, 8, 32, 1, 327, DateTimeKind.Utc).AddTicks(710), 156, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1689250290" },
                    { 254, 2, 3, 2, "Odio Id Dolorem", new DateTime(2019, 2, 10, 6, 36, 32, 892, DateTimeKind.Utc).AddTicks(8455), 254, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1457073387" },
                    { 252, 4, 3, 2, "Consequatur Repudiandae", new DateTime(2019, 2, 12, 2, 53, 57, 550, DateTimeKind.Utc).AddTicks(5008), 252, "https://loremflickr.com/320/240/casino,casinogame/any?lock=120735517" },
                    { 246, 1, 3, 2, "Fugiat", new DateTime(2019, 2, 5, 13, 4, 55, 585, DateTimeKind.Utc).AddTicks(3684), 246, "https://loremflickr.com/320/240/casino,casinogame/any?lock=243047022" },
                    { 241, 1, 1, 2, "Inventore", new DateTime(2019, 2, 14, 0, 4, 49, 598, DateTimeKind.Utc).AddTicks(8831), 241, "https://loremflickr.com/320/240/casino,casinogame/any?lock=2004192459" },
                    { 238, 6, 3, 2, "Pariatur Magni Exercitationem", new DateTime(2019, 2, 14, 5, 42, 6, 681, DateTimeKind.Utc).AddTicks(6261), 238, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1646355637" },
                    { 237, 1, 2, 2, "Quia Nobis", new DateTime(2019, 2, 13, 20, 30, 56, 883, DateTimeKind.Utc).AddTicks(9115), 237, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1107534560" },
                    { 233, 6, 2, 2, "Inventore", new DateTime(2019, 2, 9, 19, 58, 54, 98, DateTimeKind.Utc).AddTicks(4228), 233, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1646316404" },
                    { 232, 1, 1, 2, "Eos Qui", new DateTime(2019, 2, 5, 22, 7, 50, 846, DateTimeKind.Utc).AddTicks(5603), 232, "https://loremflickr.com/320/240/casino,casinogame/any?lock=239513892" },
                    { 228, 4, 3, 2, "Dolorem Laboriosam", new DateTime(2019, 2, 13, 8, 33, 42, 129, DateTimeKind.Utc).AddTicks(5333), 228, "https://loremflickr.com/320/240/casino,casinogame/any?lock=472530651" },
                    { 226, 6, 3, 2, "Excepturi Alias Minima", new DateTime(2019, 2, 8, 14, 21, 39, 386, DateTimeKind.Utc).AddTicks(8369), 226, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1282536427" },
                    { 219, 5, 3, 2, "Mollitia Omnis Itaque", new DateTime(2019, 2, 12, 11, 39, 15, 628, DateTimeKind.Utc).AddTicks(8211), 219, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1977709794" },
                    { 217, 1, 3, 2, "Dolore", new DateTime(2019, 2, 13, 14, 33, 43, 930, DateTimeKind.Utc).AddTicks(2349), 217, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1704074821" },
                    { 215, 6, 1, 2, "Esse Dolore", new DateTime(2019, 2, 9, 19, 57, 36, 788, DateTimeKind.Utc).AddTicks(7376), 215, "https://loremflickr.com/320/240/casino,casinogame/any?lock=812208741" },
                    { 263, 5, 2, 2, "Beatae Est", new DateTime(2019, 2, 11, 17, 17, 56, 270, DateTimeKind.Utc).AddTicks(6992), 263, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1868438034" },
                    { 213, 1, 3, 2, "Est Corporis Autem", new DateTime(2019, 2, 10, 14, 16, 33, 896, DateTimeKind.Utc).AddTicks(4128), 213, "https://loremflickr.com/320/240/casino,casinogame/any?lock=515823198" },
                    { 206, 4, 3, 2, "Voluptatem", new DateTime(2019, 2, 11, 0, 48, 41, 674, DateTimeKind.Utc).AddTicks(5768), 206, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1094657293" },
                    { 204, 4, 3, 2, "Reprehenderit Velit Dicta", new DateTime(2019, 2, 9, 9, 57, 24, 238, DateTimeKind.Utc).AddTicks(4773), 204, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1900914219" },
                    { 202, 2, 3, 2, "Eaque", new DateTime(2019, 2, 14, 4, 43, 56, 733, DateTimeKind.Utc).AddTicks(7498), 202, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1261481680" },
                    { 201, 6, 1, 2, "Cupiditate Iure", new DateTime(2019, 2, 10, 15, 1, 40, 625, DateTimeKind.Utc).AddTicks(1830), 201, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1452655362" },
                    { 199, 4, 2, 2, "Id Dignissimos Et", new DateTime(2019, 2, 9, 10, 30, 49, 653, DateTimeKind.Utc).AddTicks(8434), 199, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1671054438" },
                    { 198, 1, 1, 2, "Et Dolores Eum", new DateTime(2019, 2, 6, 5, 1, 19, 839, DateTimeKind.Utc).AddTicks(5574), 198, "https://loremflickr.com/320/240/casino,casinogame/any?lock=905865357" },
                    { 193, 6, 1, 2, "Exercitationem", new DateTime(2019, 2, 6, 10, 43, 11, 185, DateTimeKind.Utc).AddTicks(5352), 193, "https://loremflickr.com/320/240/casino,casinogame/any?lock=169026799" },
                    { 187, 3, 2, 2, "Aut Voluptatem", new DateTime(2019, 2, 7, 13, 53, 49, 426, DateTimeKind.Utc).AddTicks(5406), 187, "https://loremflickr.com/320/240/casino,casinogame/any?lock=538561921" },
                    { 185, 3, 2, 2, "Amet", new DateTime(2019, 2, 9, 19, 41, 25, 963, DateTimeKind.Utc).AddTicks(4257), 185, "https://loremflickr.com/320/240/casino,casinogame/any?lock=608501076" },
                    { 179, 6, 2, 2, "Et", new DateTime(2019, 2, 7, 20, 6, 38, 22, DateTimeKind.Utc).AddTicks(1452), 179, "https://loremflickr.com/320/240/casino,casinogame/any?lock=495504521" },
                    { 177, 4, 2, 2, "Quidem Magnam", new DateTime(2019, 2, 13, 13, 6, 39, 529, DateTimeKind.Utc).AddTicks(599), 177, "https://loremflickr.com/320/240/casino,casinogame/any?lock=942851825" },
                    { 168, 2, 1, 2, "Totam Temporibus", new DateTime(2019, 2, 14, 11, 4, 12, 183, DateTimeKind.Utc).AddTicks(3469), 168, "https://loremflickr.com/320/240/casino,casinogame/any?lock=1719804768" },
                    { 159, 1, 3, 2, "Quia Minima Pariatur", new DateTime(2019, 2, 8, 2, 43, 50, 923, DateTimeKind.Utc).AddTicks(1483), 159, "https://loremflickr.com/320/240/casino,casinogame/any?lock=13551699" },
                    { 211, 1, 3, 2, "Placeat Est", new DateTime(2019, 2, 4, 15, 7, 20, 294, DateTimeKind.Utc).AddTicks(3611), 211, "https://loremflickr.com/320/240/casino,casinogame/any?lock=994202166" },
                    { 495, 1, 2, 4, "Ducimus Eos", new DateTime(2019, 2, 13, 0, 55, 45, 754, DateTimeKind.Utc).AddTicks(1921), 495, "https://loremflickr.com/320/240/casino,casinogame/any?lock=270068404" }
                });

            migrationBuilder.InsertData(
                table: "tGameCollectionMap",
                columns: new[] { "GameId", "GameCollectionId" },
                values: new object[,]
                {
                    { 66, 3 },
                    { 320, 15 },
                    { 316, 8 },
                    { 316, 21 },
                    { 311, 8 },
                    { 311, 21 },
                    { 308, 21 },
                    { 330, 9 },
                    { 308, 9 },
                    { 297, 21 },
                    { 289, 22 },
                    { 271, 15 },
                    { 266, 21 },
                    { 245, 3 },
                    { 245, 15 },
                    { 303, 18 },
                    { 245, 2 },
                    { 346, 20 },
                    { 346, 8 },
                    { 428, 3 },
                    { 428, 20 },
                    { 422, 15 },
                    { 406, 2 },
                    { 406, 9 },
                    { 400, 22 },
                    { 346, 3 },
                    { 388, 14 },
                    { 373, 18 },
                    { 372, 20 },
                    { 367, 23 },
                    { 355, 21 },
                    { 352, 21 },
                    { 352, 9 },
                    { 385, 2 },
                    { 465, 3 },
                    { 236, 15 },
                    { 230, 18 },
                    { 149, 15 },
                    { 149, 8 },
                    { 149, 1 },
                    { 148, 1 },
                    { 148, 18 },
                    { 148, 14 },
                    { 149, 21 },
                    { 145, 21 },
                    { 140, 15 },
                    { 127, 18 },
                    { 118, 1 },
                    { 104, 14 },
                    { 75, 1 },
                    { 73, 9 },
                    { 145, 8 },
                    { 234, 2 },
                    { 152, 8 },
                    { 153, 15 },
                    { 230, 21 },
                    { 221, 15 },
                    { 221, 14 },
                    { 218, 23 },
                    { 195, 15 },
                    { 194, 9 },
                    { 152, 23 },
                    { 183, 18 },
                    { 163, 1 },
                    { 163, 8 },
                    { 161, 22 },
                    { 161, 1 },
                    { 158, 2 },
                    { 154, 15 },
                    { 178, 21 },
                    { 466, 14 },
                    { 480, 9 },
                    { 26, 1 },
                    { 350, 20 },
                    { 339, 20 },
                    { 334, 9 },
                    { 329, 15 },
                    { 329, 1 },
                    { 328, 18 },
                    { 353, 8 },
                    { 314, 15 },
                    { 298, 21 },
                    { 285, 9 },
                    { 274, 9 },
                    { 269, 23 },
                    { 256, 22 },
                    { 256, 9 },
                    { 310, 3 },
                    { 253, 20 },
                    { 357, 22 },
                    { 392, 20 },
                    { 487, 22 },
                    { 479, 3 },
                    { 479, 8 },
                    { 448, 1 },
                    { 447, 1 },
                    { 447, 8 },
                    { 390, 21 },
                    { 439, 14 },
                    { 436, 18 },
                    { 436, 23 },
                    { 434, 15 },
                    { 430, 14 },
                    { 398, 3 },
                    { 392, 1 },
                    { 436, 21 },
                    { 242, 9 },
                    { 224, 14 },
                    { 216, 20 },
                    { 93, 3 },
                    { 87, 15 },
                    { 78, 18 },
                    { 76, 14 },
                    { 58, 15 },
                    { 58, 1 },
                    { 106, 23 },
                    { 58, 21 },
                    { 55, 3 },
                    { 53, 3 },
                    { 48, 22 },
                    { 44, 23 },
                    { 40, 20 },
                    { 38, 2 },
                    { 58, 20 },
                    { 110, 18 },
                    { 110, 20 },
                    { 111, 9 },
                    { 212, 18 },
                    { 212, 2 },
                    { 203, 14 },
                    { 197, 21 },
                    { 197, 20 },
                    { 186, 2 },
                    { 186, 14 },
                    { 173, 15 },
                    { 160, 9 },
                    { 146, 2 },
                    { 146, 18 },
                    { 143, 2 },
                    { 128, 14 },
                    { 125, 21 },
                    { 111, 18 },
                    { 72, 15 },
                    { 71, 21 },
                    { 490, 2 },
                    { 64, 22 },
                    { 332, 2 },
                    { 336, 20 },
                    { 370, 22 },
                    { 379, 14 },
                    { 379, 20 },
                    { 389, 8 },
                    { 389, 18 },
                    { 393, 9 },
                    { 395, 15 },
                    { 408, 18 },
                    { 408, 2 },
                    { 414, 23 },
                    { 420, 18 },
                    { 442, 23 },
                    { 442, 3 },
                    { 456, 22 },
                    { 457, 15 },
                    { 56, 2 },
                    { 487, 2 },
                    { 47, 22 },
                    { 39, 8 },
                    { 39, 18 },
                    { 23, 22 },
                    { 302, 8 },
                    { 10, 21 },
                    { 493, 3 },
                    { 483, 1 },
                    { 481, 18 },
                    { 463, 23 },
                    { 458, 9 },
                    { 458, 1 },
                    { 494, 22 },
                    { 56, 3 },
                    { 300, 8 },
                    { 259, 8 },
                    { 15, 15 },
                    { 18, 23 },
                    { 27, 2 },
                    { 29, 22 },
                    { 41, 20 },
                    { 43, 14 },
                    { 46, 15 },
                    { 51, 14 },
                    { 61, 20 },
                    { 61, 18 },
                    { 74, 2 },
                    { 77, 22 },
                    { 86, 18 },
                    { 86, 8 },
                    { 88, 2 },
                    { 88, 3 },
                    { 88, 1 },
                    { 255, 15 },
                    { 244, 15 },
                    { 243, 8 },
                    { 239, 22 },
                    { 235, 20 },
                    { 227, 14 },
                    { 267, 2 },
                    { 227, 9 },
                    { 205, 18 },
                    { 189, 8 },
                    { 130, 23 },
                    { 123, 1 },
                    { 123, 21 },
                    { 119, 2 },
                    { 207, 15 },
                    { 85, 15 },
                    { 56, 8 },
                    { 122, 9 },
                    { 364, 8 },
                    { 112, 1 },
                    { 383, 2 },
                    { 384, 23 },
                    { 405, 1 },
                    { 415, 8 },
                    { 424, 2 },
                    { 424, 21 },
                    { 424, 8 },
                    { 425, 2 },
                    { 451, 2 },
                    { 453, 8 },
                    { 474, 2 },
                    { 482, 2 },
                    { 482, 23 },
                    { 484, 23 },
                    { 500, 8 },
                    { 59, 15 },
                    { 52, 23 },
                    { 22, 8 },
                    { 21, 3 },
                    { 20, 8 },
                    { 20, 20 },
                    { 327, 14 },
                    { 20, 21 },
                    { 16, 21 },
                    { 16, 14 },
                    { 14, 1 },
                    { 13, 8 },
                    { 13, 3 },
                    { 3, 14 },
                    { 19, 21 },
                    { 322, 18 },
                    { 364, 21 },
                    { 307, 18 },
                    { 126, 3 },
                    { 126, 18 },
                    { 138, 23 },
                    { 147, 2 },
                    { 150, 15 },
                    { 168, 15 },
                    { 168, 23 },
                    { 177, 2 },
                    { 177, 18 },
                    { 179, 15 },
                    { 185, 15 },
                    { 193, 8 },
                    { 318, 22 },
                    { 201, 9 },
                    { 202, 20 },
                    { 198, 20 },
                    { 204, 3 },
                    { 296, 2 },
                    { 294, 1 },
                    { 290, 14 },
                    { 202, 23 },
                    { 283, 22 },
                    { 277, 2 },
                    { 238, 14 },
                    { 283, 2 },
                    { 233, 21 },
                    { 228, 20 },
                    { 226, 15 },
                    { 217, 9 },
                    { 206, 3 },
                    { 206, 1 },
                    { 238, 23 }
                });

            migrationBuilder.InsertData(
                table: "tGameCollections",
                columns: new[] { "Id", "Name", "ParentCollectionId", "SortOrder" },
                values: new object[,]
                {
                    { 24, "Libero Collection", 3, 24 },
                    { 11, "Possimus Collection", 3, 11 },
                    { 10, "Est Collection", 3, 10 },
                    { 7, "Ut Collection", 3, 7 },
                    { 4, "Quod Collection", 3, 4 },
                    { 6, "Quia Collection", 2, 6 },
                    { 5, "Illo Collection", 2, 5 }
                });

            migrationBuilder.InsertData(
                table: "tGameCollectionMap",
                columns: new[] { "GameId", "GameCollectionId" },
                values: new object[,]
                {
                    { 91, 5 },
                    { 146, 10 },
                    { 70, 10 },
                    { 127, 10 },
                    { 317, 10 },
                    { 446, 10 },
                    { 117, 10 },
                    { 380, 10 },
                    { 469, 10 },
                    { 92, 10 },
                    { 214, 10 },
                    { 462, 10 },
                    { 439, 10 },
                    { 167, 10 },
                    { 211, 10 },
                    { 204, 10 },
                    { 298, 10 },
                    { 95, 10 },
                    { 22, 10 },
                    { 68, 10 },
                    { 300, 10 },
                    { 97, 7 },
                    { 437, 7 },
                    { 43, 7 },
                    { 295, 7 },
                    { 49, 7 },
                    { 85, 7 },
                    { 87, 7 },
                    { 44, 7 },
                    { 77, 7 },
                    { 390, 7 },
                    { 418, 7 },
                    { 55, 7 },
                    { 438, 7 },
                    { 434, 7 },
                    { 126, 7 },
                    { 196, 7 },
                    { 21, 10 },
                    { 206, 10 },
                    { 283, 10 },
                    { 310, 10 },
                    { 176, 11 },
                    { 404, 24 },
                    { 329, 24 },
                    { 222, 24 },
                    { 230, 24 },
                    { 110, 24 },
                    { 121, 24 },
                    { 427, 24 },
                    { 476, 24 },
                    { 175, 24 },
                    { 43, 24 },
                    { 17, 24 },
                    { 241, 24 },
                    { 10, 24 },
                    { 438, 24 },
                    { 131, 24 },
                    { 56, 24 },
                    { 457, 24 },
                    { 144, 24 },
                    { 174, 7 },
                    { 193, 24 },
                    { 146, 24 },
                    { 360, 11 },
                    { 211, 11 },
                    { 416, 11 },
                    { 216, 11 },
                    { 86, 11 },
                    { 345, 11 },
                    { 16, 11 },
                    { 238, 11 },
                    { 195, 11 },
                    { 212, 11 },
                    { 317, 11 },
                    { 472, 11 },
                    { 344, 11 },
                    { 54, 11 },
                    { 444, 11 },
                    { 245, 11 },
                    { 228, 11 },
                    { 11, 24 },
                    { 415, 24 },
                    { 225, 7 },
                    { 297, 7 },
                    { 206, 5 },
                    { 187, 6 },
                    { 472, 6 },
                    { 221, 6 },
                    { 93, 6 },
                    { 485, 6 },
                    { 425, 6 },
                    { 197, 6 },
                    { 267, 6 },
                    { 185, 6 },
                    { 75, 6 },
                    { 51, 6 },
                    { 149, 6 },
                    { 280, 6 },
                    { 438, 6 },
                    { 214, 6 },
                    { 448, 6 },
                    { 449, 5 },
                    { 485, 5 },
                    { 294, 5 },
                    { 347, 5 },
                    { 211, 5 },
                    { 292, 5 },
                    { 160, 5 },
                    { 66, 5 },
                    { 465, 5 },
                    { 360, 5 },
                    { 274, 5 },
                    { 315, 5 },
                    { 128, 6 },
                    { 237, 5 },
                    { 343, 5 },
                    { 175, 5 },
                    { 457, 5 },
                    { 423, 5 },
                    { 372, 5 },
                    { 185, 5 },
                    { 375, 5 },
                    { 214, 5 },
                    { 44, 5 },
                    { 194, 7 },
                    { 411, 6 },
                    { 177, 6 },
                    { 97, 4 },
                    { 203, 4 },
                    { 446, 4 },
                    { 21, 4 },
                    { 322, 4 },
                    { 95, 4 },
                    { 247, 4 },
                    { 113, 4 },
                    { 81, 4 },
                    { 410, 4 },
                    { 173, 4 },
                    { 111, 4 },
                    { 193, 4 },
                    { 192, 4 },
                    { 172, 7 },
                    { 472, 7 },
                    { 361, 4 },
                    { 63, 6 },
                    { 398, 4 },
                    { 119, 4 },
                    { 327, 6 },
                    { 31, 6 },
                    { 380, 6 },
                    { 316, 6 },
                    { 378, 6 },
                    { 215, 6 },
                    { 414, 6 },
                    { 40, 6 },
                    { 69, 4 },
                    { 483, 4 },
                    { 439, 4 },
                    { 35, 4 },
                    { 20, 4 },
                    { 43, 4 },
                    { 6, 4 },
                    { 488, 4 },
                    { 345, 4 },
                    { 369, 24 }
                });

            migrationBuilder.InsertData(
                table: "tGameCollections",
                columns: new[] { "Id", "Name", "ParentCollectionId", "SortOrder" },
                values: new object[,]
                {
                    { 17, "Et Collection", 4, 17 },
                    { 12, "Et Collection", 10, 12 },
                    { 13, "Cumque Collection", 10, 13 }
                });

            migrationBuilder.InsertData(
                table: "tGameCollectionMap",
                columns: new[] { "GameId", "GameCollectionId" },
                values: new object[,]
                {
                    { 74, 12 },
                    { 490, 13 },
                    { 44, 12 },
                    { 339, 12 },
                    { 374, 12 },
                    { 444, 12 },
                    { 398, 12 },
                    { 272, 12 },
                    { 63, 13 },
                    { 386, 12 },
                    { 189, 12 },
                    { 160, 12 },
                    { 373, 12 },
                    { 343, 12 },
                    { 470, 12 },
                    { 344, 12 },
                    { 422, 12 },
                    { 112, 12 },
                    { 402, 13 },
                    { 385, 13 },
                    { 344, 13 },
                    { 471, 13 },
                    { 118, 13 },
                    { 409, 13 },
                    { 185, 13 },
                    { 343, 13 },
                    { 90, 13 },
                    { 276, 13 },
                    { 320, 13 },
                    { 257, 13 },
                    { 382, 13 },
                    { 336, 13 },
                    { 430, 13 },
                    { 332, 13 },
                    { 135, 13 },
                    { 212, 13 },
                    { 475, 13 },
                    { 307, 13 },
                    { 111, 12 },
                    { 62, 13 },
                    { 286, 13 },
                    { 91, 12 },
                    { 327, 17 },
                    { 274, 17 },
                    { 484, 17 },
                    { 317, 17 },
                    { 147, 17 },
                    { 259, 17 },
                    { 402, 17 },
                    { 151, 17 },
                    { 255, 17 },
                    { 474, 17 },
                    { 134, 17 },
                    { 17, 17 },
                    { 1, 17 },
                    { 173, 17 },
                    { 22, 17 },
                    { 200, 17 },
                    { 228, 17 },
                    { 53, 17 },
                    { 83, 17 },
                    { 424, 17 },
                    { 151, 12 },
                    { 107, 12 },
                    { 181, 12 },
                    { 172, 12 },
                    { 349, 12 },
                    { 56, 17 },
                    { 193, 17 },
                    { 83, 12 },
                    { 251, 17 },
                    { 51, 17 },
                    { 486, 17 },
                    { 183, 17 },
                    { 146, 17 },
                    { 68, 17 },
                    { 217, 17 },
                    { 42, 17 },
                    { 445, 17 }
                });

            migrationBuilder.InsertData(
                table: "tGameCollections",
                columns: new[] { "Id", "Name", "ParentCollectionId", "SortOrder" },
                values: new object[,]
                {
                    { 16, "Ut Collection", 12, 16 },
                    { 19, "Quia Collection", 17, 19 }
                });

            migrationBuilder.InsertData(
                table: "tGameCollectionMap",
                columns: new[] { "GameId", "GameCollectionId" },
                values: new object[,]
                {
                    { 60, 19 },
                    { 393, 16 },
                    { 104, 16 },
                    { 167, 16 },
                    { 85, 16 },
                    { 473, 16 },
                    { 232, 16 },
                    { 3, 16 },
                    { 89, 16 },
                    { 260, 16 },
                    { 54, 16 },
                    { 320, 16 },
                    { 225, 16 },
                    { 422, 16 },
                    { 297, 16 },
                    { 206, 16 },
                    { 279, 16 },
                    { 224, 16 },
                    { 261, 16 },
                    { 263, 16 },
                    { 390, 16 },
                    { 118, 16 },
                    { 410, 16 },
                    { 459, 16 },
                    { 395, 16 },
                    { 100, 16 },
                    { 427, 16 },
                    { 101, 16 },
                    { 438, 16 },
                    { 445, 16 },
                    { 285, 16 },
                    { 230, 16 },
                    { 458, 16 },
                    { 244, 19 },
                    { 136, 19 },
                    { 127, 19 },
                    { 303, 19 },
                    { 115, 19 },
                    { 68, 19 },
                    { 318, 19 },
                    { 452, 19 },
                    { 296, 19 },
                    { 18, 19 },
                    { 470, 19 },
                    { 15, 19 },
                    { 273, 19 },
                    { 101, 19 },
                    { 71, 19 },
                    { 407, 19 },
                    { 81, 19 },
                    { 413, 19 },
                    { 150, 19 },
                    { 381, 19 },
                    { 20, 19 },
                    { 184, 19 },
                    { 278, 19 },
                    { 416, 19 },
                    { 225, 19 },
                    { 9, 19 },
                    { 392, 19 },
                    { 432, 19 },
                    { 125, 19 },
                    { 94, 16 },
                    { 235, 16 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 1, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 3, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 3, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 6, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 9, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 10, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 10, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 11, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 13, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 13, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 14, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 15, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 15, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 16, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 16, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 16, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 17, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 17, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 18, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 18, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 19, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 20, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 20, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 20, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 20, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 20, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 21, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 21, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 21, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 22, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 22, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 22, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 23, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 26, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 27, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 29, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 31, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 35, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 38, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 39, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 39, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 40, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 40, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 41, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 42, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 43, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 43, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 43, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 43, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 44, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 44, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 44, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 44, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 46, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 47, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 48, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 49, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 51, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 51, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 51, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 52, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 53, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 53, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 54, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 54, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 55, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 55, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 56, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 56, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 56, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 56, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 56, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 58, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 58, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 58, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 58, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 59, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 60, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 61, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 61, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 62, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 63, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 63, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 64, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 66, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 66, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 68, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 68, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 68, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 69, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 70, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 71, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 71, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 72, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 73, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 74, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 74, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 75, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 75, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 76, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 77, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 77, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 78, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 81, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 81, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 83, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 83, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 85, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 85, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 85, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 86, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 86, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 86, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 87, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 87, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 88, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 88, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 88, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 89, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 90, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 91, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 91, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 92, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 93, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 93, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 94, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 95, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 95, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 97, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 97, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 100, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 101, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 101, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 104, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 104, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 106, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 107, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 110, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 110, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 110, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 111, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 111, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 111, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 111, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 112, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 112, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 113, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 115, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 117, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 118, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 118, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 118, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 119, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 119, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 121, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 122, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 123, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 123, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 125, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 125, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 126, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 126, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 126, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 127, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 127, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 127, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 128, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 128, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 130, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 131, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 134, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 135, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 136, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 138, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 140, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 143, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 144, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 145, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 145, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 146, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 146, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 146, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 146, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 146, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 147, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 147, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 148, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 148, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 148, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 149, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 149, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 149, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 149, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 149, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 150, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 150, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 151, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 151, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 152, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 152, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 153, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 154, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 158, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 160, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 160, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 160, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 161, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 161, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 163, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 163, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 167, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 167, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 168, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 168, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 172, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 172, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 173, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 173, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 173, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 174, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 175, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 175, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 176, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 177, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 177, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 177, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 178, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 179, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 181, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 183, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 183, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 184, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 185, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 185, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 185, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 185, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 186, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 186, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 187, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 189, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 189, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 192, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 193, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 193, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 193, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 193, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 194, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 194, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 195, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 195, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 196, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 197, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 197, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 197, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 198, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 200, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 201, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 202, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 202, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 203, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 203, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 204, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 204, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 205, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 206, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 206, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 206, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 206, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 206, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 207, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 211, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 211, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 211, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 212, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 212, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 212, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 212, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 214, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 214, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 214, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 215, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 216, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 216, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 217, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 217, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 218, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 221, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 221, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 221, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 222, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 224, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 224, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 225, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 225, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 225, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 226, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 227, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 227, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 228, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 228, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 228, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 230, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 230, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 230, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 230, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 232, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 233, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 234, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 235, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 235, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 236, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 237, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 238, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 238, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 238, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 239, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 241, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 242, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 243, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 244, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 244, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 245, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 245, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 245, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 245, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 247, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 251, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 253, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 255, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 255, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 256, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 256, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 257, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 259, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 259, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 260, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 261, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 263, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 266, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 267, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 267, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 269, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 271, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 272, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 273, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 274, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 274, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 274, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 276, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 277, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 278, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 279, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 280, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 283, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 283, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 283, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 285, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 285, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 286, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 289, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 290, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 292, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 294, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 294, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 295, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 296, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 296, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 297, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 297, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 297, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 298, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 298, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 300, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 300, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 302, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 303, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 303, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 307, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 307, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 308, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 308, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 310, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 310, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 311, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 311, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 314, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 315, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 316, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 316, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 316, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 317, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 317, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 317, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 318, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 318, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 320, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 320, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 320, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 322, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 322, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 327, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 327, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 327, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 328, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 329, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 329, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 329, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 330, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 332, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 332, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 334, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 336, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 336, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 339, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 339, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 343, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 343, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 343, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 344, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 344, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 344, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 345, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 345, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 346, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 346, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 346, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 347, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 349, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 350, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 352, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 352, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 353, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 355, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 357, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 360, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 360, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 361, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 364, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 364, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 367, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 369, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 370, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 372, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 372, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 373, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 373, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 374, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 375, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 378, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 379, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 379, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 380, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 380, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 381, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 382, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 383, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 384, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 385, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 385, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 386, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 388, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 389, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 389, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 390, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 390, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 390, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 392, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 392, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 392, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 393, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 393, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 395, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 395, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 398, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 398, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 398, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 400, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 402, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 402, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 404, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 405, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 406, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 406, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 407, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 408, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 408, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 409, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 410, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 410, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 411, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 413, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 414, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 414, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 415, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 415, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 416, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 416, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 418, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 420, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 422, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 422, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 422, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 423, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 424, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 424, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 424, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 424, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 425, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 425, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 427, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 427, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 428, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 428, 20 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 430, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 430, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 432, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 434, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 434, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 436, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 436, 21 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 436, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 437, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 438, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 438, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 438, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 438, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 439, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 439, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 439, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 442, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 442, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 444, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 444, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 445, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 445, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 446, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 446, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 447, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 447, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 448, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 448, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 449, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 451, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 452, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 453, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 456, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 457, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 457, 15 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 457, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 458, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 458, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 458, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 459, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 462, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 463, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 465, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 465, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 466, 14 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 469, 10 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 470, 12 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 470, 19 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 471, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 472, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 472, 7 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 472, 11 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 473, 16 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 474, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 474, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 475, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 476, 24 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 479, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 479, 8 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 480, 9 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 481, 18 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 482, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 482, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 483, 1 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 483, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 484, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 484, 23 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 485, 5 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 485, 6 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 486, 17 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 487, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 487, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 488, 4 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 490, 2 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 490, 13 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 493, 3 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 494, 22 });

            migrationBuilder.DeleteData(
                table: "tGameCollectionMap",
                keyColumns: new[] { "GameId", "GameCollectionId" },
                keyValues: new object[] { 500, 8 });

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 57);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 65);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 67);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 79);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 80);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 82);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 84);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 96);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 98);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 99);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 102);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 103);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 105);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 108);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 109);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 114);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 116);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 120);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 124);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 129);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 132);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 133);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 137);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 139);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 141);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 142);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 155);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 156);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 157);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 159);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 162);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 164);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 165);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 166);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 169);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 170);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 171);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 180);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 182);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 188);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 190);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 191);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 199);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 208);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 209);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 210);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 213);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 219);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 220);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 223);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 229);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 231);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 240);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 246);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 248);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 249);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 250);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 252);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 254);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 258);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 262);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 264);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 265);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 268);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 270);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 275);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 281);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 282);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 284);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 287);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 288);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 291);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 293);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 299);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 301);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 304);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 305);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 306);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 309);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 312);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 313);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 319);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 321);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 323);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 324);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 325);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 326);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 331);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 333);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 335);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 337);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 338);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 340);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 341);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 342);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 348);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 351);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 354);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 356);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 358);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 359);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 362);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 363);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 365);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 366);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 368);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 371);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 376);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 377);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 387);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 391);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 394);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 396);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 397);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 399);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 401);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 403);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 412);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 417);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 419);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 421);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 426);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 429);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 431);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 433);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 435);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 440);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 441);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 443);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 450);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 454);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 455);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 460);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 461);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 464);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 467);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 468);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 477);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 478);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 489);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 491);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 492);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 495);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 496);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 497);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 498);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 499);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 51);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 56);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 58);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 59);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 60);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 61);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 62);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 63);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 64);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 66);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 68);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 70);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 71);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 72);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 73);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 74);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 75);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 76);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 77);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 78);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 81);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 83);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 85);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 86);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 87);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 88);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 89);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 90);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 91);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 92);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 93);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 94);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 95);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 97);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 100);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 101);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 104);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 106);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 107);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 110);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 111);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 112);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 113);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 115);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 117);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 118);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 119);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 121);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 122);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 123);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 125);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 126);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 127);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 128);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 130);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 131);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 134);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 135);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 136);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 138);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 140);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 143);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 144);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 145);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 146);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 147);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 148);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 149);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 150);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 151);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 152);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 153);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 154);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 158);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 160);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 161);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 163);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 167);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 168);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 172);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 173);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 174);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 175);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 176);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 177);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 178);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 179);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 181);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 183);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 184);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 185);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 186);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 187);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 189);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 192);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 193);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 194);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 195);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 196);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 197);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 198);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 200);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 201);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 202);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 203);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 204);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 205);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 206);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 207);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 211);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 212);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 214);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 215);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 216);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 217);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 218);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 221);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 222);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 224);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 225);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 226);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 227);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 228);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 230);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 232);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 233);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 234);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 235);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 236);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 237);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 238);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 239);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 241);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 242);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 243);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 244);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 245);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 247);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 251);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 253);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 255);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 256);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 257);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 259);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 260);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 261);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 263);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 266);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 267);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 269);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 271);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 272);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 273);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 274);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 276);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 277);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 278);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 279);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 280);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 283);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 285);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 286);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 289);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 290);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 292);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 294);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 295);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 296);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 297);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 298);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 300);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 302);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 303);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 307);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 308);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 310);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 311);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 314);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 315);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 316);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 317);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 318);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 320);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 322);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 327);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 328);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 329);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 330);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 332);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 334);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 336);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 339);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 343);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 344);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 345);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 346);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 347);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 349);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 350);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 352);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 353);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 355);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 357);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 360);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 361);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 364);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 367);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 369);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 370);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 372);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 373);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 374);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 375);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 378);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 379);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 380);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 381);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 382);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 383);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 384);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 385);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 386);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 388);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 389);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 390);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 392);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 393);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 395);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 398);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 400);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 402);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 404);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 405);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 406);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 407);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 408);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 409);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 410);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 411);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 413);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 414);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 415);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 416);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 418);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 420);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 422);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 423);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 424);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 425);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 427);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 428);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 430);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 432);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 434);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 436);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 437);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 438);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 439);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 442);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 444);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 445);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 446);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 447);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 448);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 449);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 451);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 452);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 453);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 456);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 457);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 458);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 459);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 462);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 463);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 465);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 466);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 469);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 470);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 471);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 472);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 473);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 474);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 475);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 476);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 479);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 480);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 481);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 482);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 483);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 484);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 485);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 486);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 487);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 488);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 490);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 493);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 494);

            migrationBuilder.DeleteData(
                table: "tGames",
                keyColumn: "Id",
                keyValue: 500);

            migrationBuilder.DeleteData(
                table: "tGameCategories",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "tGameCategories",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "tGameCategories",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "tGameCategories",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "tGameCategories",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "tGameCategories",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "tGamePlatforms",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "tGamePlatforms",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "tGamePlatforms",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "tGameProviders",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "tGameProviders",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "tGameProviders",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "tGameProviders",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "tGameCollections",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
