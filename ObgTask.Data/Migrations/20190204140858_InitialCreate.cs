﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ObgTask.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tGameCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tGameCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tGameCollections",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    ParentCollectionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tGameCollections", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tGameCollections_tGameCollections_ParentCollectionId",
                        column: x => x.ParentCollectionId,
                        principalTable: "tGameCollections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tGamePlatforms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tGamePlatforms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tGamePlaySessions",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 449, nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    StartedAtTime = table.Column<DateTimeOffset>(nullable: false),
                    ExpiresAtTime = table.Column<DateTimeOffset>(nullable: false),
                    GameId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tGamePlaySessions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tGameProviders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tGameProviders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tGames",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    ReleaseDate = table.Column<DateTime>(nullable: true),
                    ThumbnailUrl = table.Column<string>(maxLength: 2048, nullable: true),
                    GameCategoryId = table.Column<int>(nullable: false),
                    GamePlatformId = table.Column<int>(nullable: false, defaultValue: 1),
                    GameProviderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tGames", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tGames_tGameCategories_GameCategoryId",
                        column: x => x.GameCategoryId,
                        principalTable: "tGameCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tGames_tGamePlatforms_GamePlatformId",
                        column: x => x.GamePlatformId,
                        principalTable: "tGamePlatforms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tGames_tGameProviders_GameProviderId",
                        column: x => x.GameProviderId,
                        principalTable: "tGameProviders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tRoleClaims_tRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "tRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tUserClaims_tUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "tUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_tUserLogins_tUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "tUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_tUserRoles_tRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "tRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tUserRoles_tUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "tUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_tUserTokens_tUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "tUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tGameCollectionMap",
                columns: table => new
                {
                    GameId = table.Column<int>(nullable: false),
                    GameCollectionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tGameCollectionMap", x => new { x.GameId, x.GameCollectionId });
                    table.ForeignKey(
                        name: "FK_tGameCollectionMap_tGameCollections_GameCollectionId",
                        column: x => x.GameCollectionId,
                        principalTable: "tGameCollections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tGameCollectionMap_tGames_GameId",
                        column: x => x.GameId,
                        principalTable: "tGames",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tGameCollectionMap_GameCollectionId",
                table: "tGameCollectionMap",
                column: "GameCollectionId");

            migrationBuilder.CreateIndex(
                name: "IX_tGameCollections_ParentCollectionId",
                table: "tGameCollections",
                column: "ParentCollectionId");

            migrationBuilder.CreateIndex(
                name: "IX_tGamePlaySessions_ExpiresAtTime",
                table: "tGamePlaySessions",
                column: "ExpiresAtTime")
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_tGames_GameCategoryId",
                table: "tGames",
                column: "GameCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_tGames_GamePlatformId",
                table: "tGames",
                column: "GamePlatformId");

            migrationBuilder.CreateIndex(
                name: "IX_tGames_GameProviderId",
                table: "tGames",
                column: "GameProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_tRoleClaims_RoleId",
                table: "tRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "tRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_tUserClaims_UserId",
                table: "tUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_tUserLogins_UserId",
                table: "tUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_tUserRoles_RoleId",
                table: "tUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "tUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "tUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tGameCollectionMap");

            migrationBuilder.DropTable(
                name: "tGamePlaySessions");

            migrationBuilder.DropTable(
                name: "tRoleClaims");

            migrationBuilder.DropTable(
                name: "tUserClaims");

            migrationBuilder.DropTable(
                name: "tUserLogins");

            migrationBuilder.DropTable(
                name: "tUserRoles");

            migrationBuilder.DropTable(
                name: "tUserTokens");

            migrationBuilder.DropTable(
                name: "tGameCollections");

            migrationBuilder.DropTable(
                name: "tGames");

            migrationBuilder.DropTable(
                name: "tRoles");

            migrationBuilder.DropTable(
                name: "tUsers");

            migrationBuilder.DropTable(
                name: "tGameCategories");

            migrationBuilder.DropTable(
                name: "tGamePlatforms");

            migrationBuilder.DropTable(
                name: "tGameProviders");
        }
    }
}
