﻿using System.Linq;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ObgTask.Data.Contracts;
using ObgTask.Data.DataAccess;
using ObgTask.Data.DataAccess.Entities;
using ObgTask.Data.Repositories;
using ObgTask.Data.Services;
using JwtIssuerOptions = ObgTask.Data.Contracts.Options.JwtIssuerOptions;

namespace ObgTask.Data.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterDataServices(this IServiceCollection services, IConfiguration configuration)
        {
            string dbConnectionString = configuration.GetConnectionString("SqlDatabase");
            string assemblyName = typeof(ApplicationDbContext).Assembly.FullName.Split(',').First();

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(dbConnectionString,
                    optionsBuilder => { optionsBuilder.MigrationsAssembly(assemblyName); });
            });

            services.AddIdentityCore<AppUser>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
                .AddUserManager<UserManager<AppUser>>()
                .AddSignInManager<SignInManager<AppUser>>();

            services.AddScoped<IUnitOfWork, UnitOfWork<ApplicationDbContext>>();

            // Repositories
            services.AddScoped<GameRepository>();
            services.AddScoped<GamePlayRepository>();
            services.AddScoped<GameCollectionRepository>();
            services.AddScoped<GameCollectionMapRepository>();

            // Services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IGameDataService, GameDataService>();
            services.AddScoped<IGamePlayService, GamePlayService>();

            return services;
        }

        public static IServiceCollection ConfigureAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            var jwtOptions = configuration.GetSection("JwtIssuerOptions");
            services.Configure<JwtIssuerOptions>(jwtOptions);

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 0;

                // Lockout settings.
                options.Lockout.AllowedForNewUsers = false;

                // AppUser settings.
                options.User.RequireUniqueEmail = true;
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultSignOutScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
            {
                var jwt = jwtOptions.Get<JwtIssuerOptions>();
                options.Audience = jwt.Audience;
                options.RequireHttpsMetadata = true;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = jwt.Issuer,
                    ValidAudience = jwt.Audience,
                    IssuerSigningKey = jwt.SigningCredentials.Key
                };
            });

            return services;
        }
    }
}
