﻿using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ObgTask.Data.Contracts.Models;
using ObgTask.Data.Contracts.Models.GameData;
using ObgTask.Data.Contracts.Models.GamePlay;
using ObgTask.Data.DataAccess.Entities;
using ObgTask.Data.Mappings.Converters;

namespace ObgTask.Data.Mappings
{
    public class DataMappingProfile : Profile
    {
        public override string ProfileName => GetType().Name;

        public DataMappingProfile()
        {
            CreateMap<GameProviderDao, GameProvider>();
            CreateMap<GamePlatformDao, GamePlatform>();
            CreateMap<GameCategoryDao, GameCategory>();

            CreateMap<GameProviderDao, IdNamePair>();
            CreateMap<GamePlatformDao, IdNamePair>();
            CreateMap<GameCategoryDao, IdNamePair>();
            CreateMap<GameCollectionDao, IdNamePair>();

            CreateMap<GameCollectionDao, GameCollection>()
                .ForMember(dest => dest.ParentId, 
                    opt => opt.MapFrom(src => src.ParentCollectionId.GetValueOrDefault(0)))
                .ForMember(dest => dest.Games,
                    opt => opt.MapFrom(src => src.GameCollectionMaps.Select(x => x.GameId)));

            CreateMap<GameDao, Game>().ForMember(dest => dest.Collections, 
                opt => opt.MapFrom(src => src.GameCollectionsMaps.Select(x => x.GameCollectionId)));

            CreateMap<GamePlaySessionDao, StartGameResponse>();

            CreateMap(typeof(IPagedList<>), typeof(IPagedList<>))
                .ConvertUsing(typeof(PagedListConverter<,>));
        }
    }
}
