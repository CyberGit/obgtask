﻿using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ObgTask.Data.Contracts.Models;

namespace ObgTask.Data.Mappings.Converters
{
    public class PagedListConverter<TSrc, TDest> : ITypeConverter<IPagedList<TSrc>, IPagedList<TDest>> 
    {
        public IPagedList<TDest> Convert(IPagedList<TSrc> source, IPagedList<TDest> destination, ResolutionContext context)
        {
            return new PagedListDto<TDest>
            {
                Items = source.Items.Select(sourceItem => context.Mapper.Map<TDest>(sourceItem)).ToList(),
                IndexFrom = source.IndexFrom,
                PageSize = source.PageSize,
                PageIndex = source.PageIndex,
                TotalCount = source.TotalCount,
                TotalPages = source.TotalPages
            };
        }
    }
}
