﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using Microsoft.EntityFrameworkCore;
using ObgTask.Common.Extensions;
using ObgTask.Data.Contracts.Enums;
using ObgTask.Data.DataAccess.Entities;

namespace ObgTask.Data.Seeds
{
    internal static class GameDataSeeder
    {
        internal static void SeedGameData(this ModelBuilder builder)
        {
            GenerateFakeData();

            builder.Entity<GameProviderDao>(entity =>
            {
                entity.HasData(Providers);
            });

            builder.Entity<GameCategoryDao>(entity =>
            {
                entity.HasData(Categories);
            });
            builder.Entity<GamePlatformDao>(entity =>
            {
                entity.HasData(Platforms);
            });
            builder.Entity<GameDao>(entity =>
            {
                entity.HasData(_games.ToArray());
            });
            builder.Entity<GameCollectionDao>(entity =>
            {
                entity.HasData(_gameCollections.ToArray());
            });
            builder.Entity<GameCollectionMap>(entity =>
            {
                entity.HasData(_gameCollectionMaps.ToArray());
            });
        }

        private static readonly GameProviderDao[] Providers = new[]
        {
            new GameProviderDao { Id = 1, Name = "NetEntCasino" },
            new GameProviderDao { Id = 2, Name = "QuickspinCasino" },
            new GameProviderDao { Id = 3, Name = "NyxCasino" },
            new GameProviderDao { Id = 4, Name = "Evolution" }
        };

        private static readonly GameCategoryDao[] Categories = new[]
        {
            new GameCategoryDao { Id = 1, Name = "Classic Slots" },
            new GameCategoryDao { Id = 2, Name = "Video Slots" },
            new GameCategoryDao { Id = 3, Name = "Roulette" },
            new GameCategoryDao { Id = 4, Name = "Live Roulette" },
            new GameCategoryDao { Id = 5, Name = "Poker" },
            new GameCategoryDao { Id = 6, Name = "Blackjack" }
        };

        private static readonly GamePlatformDao[] Platforms = new[]
        {
            new GamePlatformDao { Id = 1, Type = GamePlatformType.Unknown },
            new GamePlatformDao { Id = 2, Type = GamePlatformType.Desktop },
            new GamePlatformDao { Id = 3, Type = GamePlatformType.Mobile }
        };

        private static List<GameCollectionDao> _gameCollections;
        private static List<GameCollectionMap> _gameCollectionMaps;
        private static List<GameDao> _games;

        private static void GenerateFakeData()
        {
            const int amountToGenerate = 25;
            var collectionIndex = 1;
            var tmpGameCollection = new List<GameCollectionDao>(amountToGenerate);

            for (int i = collectionIndex; i < amountToGenerate; i++)
            {
                Faker<GameCollectionDao> fakerMainCollections = new Faker<GameCollectionDao>()
                    .CustomInstantiator(x => new GameCollectionDao())
                    .RuleFor(o => o.Id, f => collectionIndex++)
                    .RuleFor(o => o.ParentCollectionId, f => GetParentId(f, tmpGameCollection))
                    .RuleFor(o => o.Name, f => $"{f.Lorem.Word()} collection".ToTitleCase())
                    .RuleFor(o => o.SortOrder, f => (collectionIndex - 1));

                int? GetParentId(Faker faker, List<GameCollectionDao> tmpGameCollections)
                {
                    const int maxRnd = 1000;
                    int randomNumber = new Random(DateTime.UtcNow.Millisecond).Next(1, maxRnd);
                    // SubCollection
                    if (tmpGameCollections.Any() && randomNumber >= maxRnd / 5)
                        return faker.PickRandom(tmpGameCollections).Id;

                    // MainCollection
                    return null;
                }

                tmpGameCollection.AddRange(fakerMainCollections.Generate(1));
            }

            var gameIndex = 1;
            Faker<GameDao> fakerGames = new Faker<GameDao>()
                .CustomInstantiator(i => new GameDao())
                .RuleFor(o => o.Id, f => gameIndex++)
                .RuleFor(o => o.Name, 
                    f => $"{ string.Join(' ', f.Lorem.Words(f.Random.Int(1, 3)))}".ToTitleCase())
                .RuleFor(o => o.GamePlatformId, f => f.PickRandom(Platforms).Id)
                .RuleFor(o => o.GameCategoryId, f => f.PickRandom(Categories).Id)
                .RuleFor(o => o.GameProviderId, f => f.PickRandom(Providers).Id)
                .RuleFor(o => o.ReleaseDate,
                    f => f.Date.Between(start: DateTime.UtcNow, end: DateTime.UtcNow.AddDays(10)))
                .RuleFor(o => o.SortOrder, f => gameIndex - 1)
                .RuleFor(o => o.ThumbnailUrl, f => f.Image.LoremFlickrUrl(keywords:"casino, casinogame"));

            _games = fakerGames.Generate(500);
            _gameCollections = tmpGameCollection;

            Faker<GameCollectionMap> fakerGamesCollectionMaps = new Faker<GameCollectionMap>()
                .CustomInstantiator(i => new GameCollectionMap())
                .RuleFor(o => o.GameId, f => f.PickRandom(_games).Id)
                .RuleFor(o => o.GameCollectionId, f => f.PickRandom(_gameCollections).Id);

            var gameCollectionMapsTmp = fakerGamesCollectionMaps.Generate( 600 );

            // Filter out items that have same ids
            _gameCollectionMaps = gameCollectionMapsTmp.Distinct(new CollectionEqualityComparer()).ToList();
        }

        private class CollectionEqualityComparer : IEqualityComparer<GameCollectionMap>
        {
            public bool Equals(GameCollectionMap x, GameCollectionMap y)
            {
                return x.GameId == y.GameId && x.GameCollectionId == y.GameCollectionId;
            }

            public int GetHashCode(GameCollectionMap obj)
            {
                int nameHash = obj.GameId.GetHashCode();

                return nameHash ^ obj.GameId ^ obj.GameCollectionId;
            }
        }

    }
}

