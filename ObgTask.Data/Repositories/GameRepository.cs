﻿using Microsoft.EntityFrameworkCore;
using ObgTask.Data.DataAccess;
using ObgTask.Data.DataAccess.Entities;

namespace ObgTask.Data.Repositories
{
    internal class GameRepository : Repository<GameDao>
    {
        public GameRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
