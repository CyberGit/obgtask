﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ObgTask.Data.DataAccess;
using ObgTask.Data.DataAccess.Entities;

namespace ObgTask.Data.Repositories
{
    internal class GameCollectionMapRepository : Repository<GameCollectionMap>
    {
        public GameCollectionMapRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<IGrouping<int, GameCollectionMap>>> GetGroupedGamesListMapAsync()
        {
            List<IGrouping<int, GameCollectionMap>> gamesList = await _dbSet
                .OrderBy(x => x.GameCollectionId)
                .GroupBy(key => key.GameCollectionId, item => item)
                .AsNoTracking().ToListAsync();
            return gamesList;
        }
    }
}
