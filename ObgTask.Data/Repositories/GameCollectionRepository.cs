﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ObgTask.Data.DataAccess;
using ObgTask.Data.DataAccess.Entities;

namespace ObgTask.Data.Repositories
{
    internal class GameCollectionRepository : Repository<GameCollectionDao>
    {
        public GameCollectionRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<GameCollectionDao>> GetGameCollectionsAsync()
        {
            var collectionsList = await _dbSet.FromSql(
                $@"WITH CollectionsCTE ( Id, Name, SortOrder, ParentCollectionId, [Level] ) AS (
                        SELECT Parent.Id, Parent.Name, Parent.SortOrder, Parent.ParentCollectionId, 0
                        FROM dbo.tGameCollections AS Parent
                        WHERE Parent.ParentCollectionId IS NULL
                        UNION ALL
                        SELECT Child.Id, Child.Name, Child.SortOrder, Child.ParentCollectionId, CollectionsCTE.[Level] + 1
                        FROM dbo.tGameCollections AS Child
                        JOIN CollectionsCTE ON Child.ParentCollectionId = CollectionsCTE.Id
                        ) SELECT * FROM CollectionsCTE").AsNoTracking().ToListAsync();
            return collectionsList;
        }
    }
}
