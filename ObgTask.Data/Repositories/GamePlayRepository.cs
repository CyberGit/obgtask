﻿using Microsoft.EntityFrameworkCore;
using ObgTask.Data.DataAccess;
using ObgTask.Data.DataAccess.Entities;

namespace ObgTask.Data.Repositories
{
    internal class GamePlayRepository : Repository<GamePlaySessionDao>
    {
        public GamePlayRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
