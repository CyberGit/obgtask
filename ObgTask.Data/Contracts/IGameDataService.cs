﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ObgTask.Data.Contracts.Models.GameData;

namespace ObgTask.Data.Contracts
{
    public interface IGameDataService
    {
        Task<Game> GetGameAsync(int gameId);
        Task<IPagedList<Game>> GetGamesAsync(int? pageIndex, int? pageSize);
        Task<GameCollection> GetCollectionAsync(int collectionId, bool excludeSubCollections = false);
        Task<IPagedList<GameCollection>> GetRootCollectionsAsync(int? pageIndex, int? pageSize);
        Task<IPagedList<GameCollection>> GetRootCollectionsEfLazyAsync(int? pageIndex, int? pageSize);
    }
}
