﻿using System.Threading.Tasks;
using ObgTask.Data.Contracts.Models.GamePlay;

namespace ObgTask.Data.Contracts
{
    public interface IGamePlayService
    {
        Task<StartGameResponse> StartGame(StartGameRequest request);
    }
}
