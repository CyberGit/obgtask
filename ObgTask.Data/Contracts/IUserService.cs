﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using ObgTask.Data.Contracts.Models;
using ObgTask.Data.DataAccess.Entities;

namespace ObgTask.Data.Contracts
{
    public interface IUserService
    {
        UserManager<AppUser> UserManager { get; }
        SignInManager<AppUser> SignInManager { get; }
        Task<JsonWebToken> GetTokenAsync(AppUser appUser);
    }
}
