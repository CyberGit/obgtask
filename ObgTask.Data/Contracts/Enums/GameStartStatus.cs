﻿using System.Diagnostics.CodeAnalysis;

// ReSharper disable IdentifierTypo
namespace ObgTask.Data.Contracts.Enums
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum GameStartStatus
    {
        GAME_SESSION_START_SUCCESS,
        GAME_SESSION_START_ERROR,
        GAME_SESSION_EXISTS_ERROR,
    }
}