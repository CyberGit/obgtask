﻿namespace ObgTask.Data.Contracts.Enums
{
    public enum GamePlatformType
    {
        Unknown = 1,
        Desktop,
        Mobile
    }
}
