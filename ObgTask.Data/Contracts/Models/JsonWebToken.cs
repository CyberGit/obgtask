﻿namespace ObgTask.Data.Contracts.Models
{
    public sealed class JsonWebToken
    {
        public string Token { get; }
        public int ValidForMinutes { get; }

        public JsonWebToken(string token, int validForMinutes)
        {
            Token = $"Bearer {token}";
            ValidForMinutes = validForMinutes;
        }
    }
}