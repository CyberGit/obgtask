﻿using System;
using System.Collections.Generic;

namespace ObgTask.Data.Contracts.Models.GameData
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string ThumbnailUrl { get; set; }

        public IdNamePair GameCategory { get; set; }
        public IdNamePair GamePlatform { get; set; }
        public IdNamePair GameProvider { get; set; }

        public ICollection<int> Collections { get; set; }
    }
}
