﻿using System.Collections.Generic;

namespace ObgTask.Data.Contracts.Models.GameData
{
    public class GameCollection
    { 
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int ParentId { get; set; }
        public bool IsRoot => ParentId == 0;
        public ICollection<int> Games { get; set; }
        public ICollection<GameCollection> SubCollections { get; set; }
    }
}
