﻿using System.Collections.Generic;
using ObgTask.Data.Contracts.Enums;

namespace ObgTask.Data.Contracts.Models.GameData
{
    public class GamePlatform
    {
        public int Id { get; set; }
        public GamePlatformType Type { get; set; }
        public ICollection<Game> Games { get; set; } = new List<Game>();
    }
}
