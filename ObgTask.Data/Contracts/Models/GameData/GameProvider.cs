﻿using System.Collections.Generic;

namespace ObgTask.Data.Contracts.Models.GameData
{
    public class GameProvider
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Game> Games { get; set; } = new List<Game>();
    }
}
