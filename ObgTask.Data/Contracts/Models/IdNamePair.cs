﻿namespace ObgTask.Data.Contracts.Models
{
    public class IdNamePair
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
