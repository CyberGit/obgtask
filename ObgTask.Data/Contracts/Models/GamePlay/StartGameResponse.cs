﻿using System;
using ObgTask.Data.Contracts.Enums;
using ObgTask.Data.DataAccess.Entities;

namespace ObgTask.Data.Contracts.Models.GamePlay
{
    public class StartGameResponse
    {
        private static readonly string[] SomeDemoGameUrls = {
            "https://bmlcw.playngonetwork.com/casino/GameLoader?pid=196&gid=bookofdead&gameId=310&lang=en_GB&practice=1&channel=desktop&div=divGameClient&width=&height=&user=practice&password=&ctx=&demo=2&brand=&lobby=&rccurrentsessiontime=0&rcintervaltime=0&rcaccounthistoryurl=&rccontinueurl=&rcexiturl=&rchistoryurlmode=&autoplaylimits=0&autoplayreset=0&callback=&rcmga=&resourcelevel=0&hasjackpots=False&country=&pauseplay=&playlimit=&selftest=&sessiontime=",
            "https://bmlcw.playngonetwork.com/casino/GameLoader?pid=196&gid=hugotwo&gameId=346&lang=en_GB&practice=1&channel=desktop&div=divGameClient&width=&height=&user=practice&password=&ctx=&demo=2&brand=&lobby=&rccurrentsessiontime=0&rcintervaltime=0&rcaccounthistoryurl=&rccontinueurl=&rcexiturl=&rchistoryurlmode=&autoplaylimits=0&autoplayreset=0&callback=&rcmga=&resourcelevel=0&hasjackpots=False&country=&pauseplay=&playlimit=&selftest=&sessiontime="
        };

        public int GameId { get; }
        public Guid GameSessionId { get; }
        public DateTimeOffset ExpiresAt { get; }
        public GameStartStatus Status { get; }
        public bool IsSuccessful => Status == GameStartStatus.GAME_SESSION_START_SUCCESS;

        public string GameUrl
        {
            get
            {
                if (!IsSuccessful) return string.Empty;
                int random = new Random(DateTime.UtcNow.Millisecond).Next(0, SomeDemoGameUrls.Length);
                return SomeDemoGameUrls[random];
            }
        }

        internal StartGameResponse(GamePlaySessionDao session, GameStartStatus status)
        {
            Status = status;
            if (session != null)
            {
                GameId = session.GameId;
                GameSessionId = session.Id;
                ExpiresAt = session.ExpiresAtTime;
            }
        }
    }
}
