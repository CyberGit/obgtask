﻿using System;
using ObgTask.Data.DataAccess.Entities;

namespace ObgTask.Data.Contracts.Models.GamePlay
{
    public class StartGameRequest
    {
        public Guid UserId { get; set; }
        public int GameId { get; set; }
    }
}
