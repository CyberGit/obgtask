# Technical Task

You are required to deliver a RESTful Web API for an online casino. This API will be able to interact in various ways with a source of casino games in a way that any client can connect to the API and consume these games, thereby becoming a casino lobby.

## Requirements

The RESTful Web API needs to have the following features implemented:

- Authentication and authorization
- List of games and game collections
- Starting a game session

### Authentication

The RESTful API needs to provide an endpoint to authenticate users by supplying a username and password. The API must check the supplied credentials against a persistent data store and response accordingly with either:

- A failure response with a reason for the failure
- A success response containing a security token

The API client will store the security token locally and used in future calls to the Web API.  Standard HTTP headers will be used to transmit the security token back to the RESTful API.

### Games

Game and game collections are the primary resources managed by this RESTful API.  The following is a list of all the information the API should provide to the consumer when it requests a game:

- Id
- Display name
- Display index
- release date of the game
- game category (classic slots, video slots, roulette, live roulette, poker, blackjack)
- A thumbnail
- List of devices the game is available on (desktop, mobile)
- A list of collections that the game is a member of.

Game collections are a hierarchical structure to group games together.  A game collection would have the following details:

- Game Collection Id
- Display name
- Display index
- List of game ids that are part of this collection
- List of sub collections

### API Consumer Activities

The API consumer should be able to execute the following actions against this API.

- List all games
- List all game collections
- Get a single game or game collection
- Start a new game session ( **only authenticated users are allowed to do this** )

In all cases, whether it&#39;s a success or a failure, the API needs to respond with the proper HTTP headers and body.

### Starting a game session

Only an authenticated user can start a game session.  When a game session is started, the API needs to provide:

1. URL for the game - the API consumer will use this to render the game on the user&#39;s display
2. The session id

# Bonus Requirements

- Support pagination on requests returning a list
- if a user is already playing a game, and he tries to launch that same game from a new request, the API does not allow them.